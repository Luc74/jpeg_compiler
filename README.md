# Notre encodeur JPEG à nous

Bienvenue sur la page d'accueil de _votre_ projet JPEG, un grand espace de liberté, sous le regard bienveillant de vos enseignants préférés.
Le sujet sera disponible dès le lundi 2 mai à l'adresse suivante : [https://formationc.pages.ensimag.fr/projet/jpeg/jpeg/](https://formationc.pages.ensimag.fr/projet/jpeg/jpeg/).

Vous pouvez reprendre cette page d'accueil comme bon vous semble, mais elle devra au moins comporter les infos suivantes **avant la fin de la première semaine (vendredi 6 mai)** :

1. des informations sur le découpage des fonctionnalités du projet en modules, en spécifiant les données en entrée et sortie de chaque étape ;
2. (au moins) un dessin des structures de données de votre projet (format libre, ça peut être une photo d'un dessin manuscrit par exemple) ;
3. une répartition des tâches au sein de votre équipe de développement, comportant une estimation du temps consacré à chacune d'elle (là encore, format libre, du truc cracra fait à la main, au joli Gantt chart).

Rajouter **régulièrement** des informations sur l'avancement de votre projet est aussi **une très bonne idée** (prendre 10 min tous les trois chaque matin pour résumer ce qui a été fait la veille, établir un plan d'action pour la journée qui commence et reporter tout ça ici, par exemple).

- Bien former ses messages de commits : [https://www.conventionalcommits.org/en/v1.0.0/](https://www.conventionalcommits.org/en/v1.0.0/) ;
- Problème relationnel au sein du groupe ? Contactez [Pascal](https://fr.wikipedia.org/wiki/Pascal,_le_grand_fr%C3%A8re) !
- Besoin de prendre l'air ? Le [Mont Rachais](https://fr.wikipedia.org/wiki/Mont_Rachais) est accessible à pieds depuis la salle E301 !
- Un peu juste sur le projet à quelques heures de la deadline ? Le [Montrachet](https://www.vinatis.com/achat-vin-puligny-montrachet) peut faire passer l'envie à vos profs de vous mettre une tôle !

# **INFORMATIONS**

## Découpage du projet en modules

Voici les différents modules utilisés :
- **AC_RLE** : permet d'effectuer l'encodage RLE des différents coefficients AC. En entrée : la valeur du pixel considéré, le nombre de zéro, le nombre de balises zéro, et un booléen indiquant si on est en fin de bloc. En sortie : renvoie la structure AC_RLE correspondante;
- **codage_DC** : contient : 
    * une fonction _magnitude_, renvoyant la classe de magnitude d'un uint32_t fourni;
    * une fonction _index_ qui renvoie, grâce au coefficient et à la magnitude de l'élément, l'index de l'élément dans sa classe de magnitude;
    * une fonction _codage_DC_ qui renvoie la composante DC du pixel grâce à son ancienne valeur (et à la fonction _magnitude_).
- **dct** : contient :
    * une fonction _decalage_, qui prend en entrée une structure Bloc et qui enlève 128 à chaque pixel du bloc, et renvoie ce même bloc directement modifié;
    * une fonction _calcul_C_, qui permet de calculer les coefficients présents devant la somme dans la transformée de Fourier (grâce à l'indice du coefficient);
    * une fonction _tab_cosinus_, qui permet de renvoyer un tableau contenant les valeurs de cosinus utilisées, sans avoir à les recalculer à chaque fois (prend, en entrée, la taille du bloc, même si dans notre cas, nous avons une taille fixe de 8x8);
    * une fonction _calcul_dct_pixel_ligne_, qui prend, en entrée, le bloc, la ligne et la colonne du pixel considéré, la taille du bloc, ainsi que la table des cosinus pré-calculés, et renvoie le flottant correspondant à la DCT 1D de notre pixel (pour la ligne);
    * une fonction _calcul_dct_pixel_colonne_, qui fonctionne comme _calcul_dct_pixel_ligne_, mais, cette fois, avec la DCT 1D sur les colonnes;
    * une fonction calcul_dct, qui prend, en entrée, le bloc considéré et le tableau des cosinus pré-calculés, et renvoie un NOUVEAU bloc correspondant à la DCT 2D du bloc initial.
- **decoupage_bloc** : contient :
    * une fonction _creer_bloc_, qui prend, en entrée, les uint32_t largeur et hauteur, et renvoie un pointeur vers la structure de Bloc ainsi créée;
    * une fonction _affiche_bloc_, qui permet d'afficher les valeurs du Bloc saisi en entrée;
    * une fonction _free_bloc_, qui permet de libérer la mémoire du Bloc saisi en entrée;
    * une fonction _nouvelle_dim_, qui, à partir de la dimension initiale et de la dimension d'un bloc, renvoie la dimension (en blocs, pour une dimension) de l'image issue de l'agrandissement;
    * une fonction _extension_, qui, à partir de la dimension initiale et de l'indice saisi en entrée, renvoie l'indice du pixel considéré dans l'image étendue;
    * une fonction _recuperation_bloc_, qui, à partir de l'image originale, de la hauteur et de la largeur d'un bloc, des coordonnées de ce bloc et de l'entier Y_Cb_Cr, récupère les blocs depuis l'image originale et les renvoie via un pointeur vers une structure Bloc.
- **ecrire_image_jpeg** : contient :
    * une fonction _ecrit_en_tete_, qui prend, en entrée, notre fichier, l'image, et deux entiers echantillon et taille_table, qui permet d'écrit l'en-tête de notre fichier JPEG;
    * une fonction _ecrit_donner_, qui prend, en entrée, le fichier, la ligne de pixels, l'ancien pixel, le nombre de pixels et le type d'image (noir et blanc VS couleur), qui permet d'écrire les données brutes qui lui sont fournies;
    * une fonction _ecri_image_, qui prend, en entrée, notre fichier, l'image, les différents coefficients d'échantillonage h1, h2, h3, v1, v2, v3, et le nombre de blocs horizontal/vertical. Elle permet d'effectuer l'écriture de tous les éléments de notre fichier (en utilisant les fonctions précédentes).
- **ImageYCbCr** : contient :
    * une fonction _init_image_, qui permet, grâce aux nombre de lignes, au nombre de colonnes, et à un entier indiquant si l'on a une image couleur ou non, de créer une nouvelle image (struct Image*);
    * une fonction _affiche_, qui permet d'afficher les éléments d'une image;
    * une fonction _free_image_, qui permet de libérer la mémoire pour une image considérée;
    * une fonction _chargement_RGB_YCbCr_, qui, à partir d'un fichier PPM, créé la structure d'image correspondate;
    * une fonction _IsLittleEndian_, qui permet de dire si l'on est au format "Little Endian" ou "Big Endian", grâce à un entier;
    * une fonction _ecrit_2octet_, qui écrit le char16_t dans le fichier passé en argument;
    * une fonction _ecrit_4octet_, qui écrit le char32_t dans le fichier passé en argument;
    * une fonction _ecrit_balise_SOI_, qui écrit la balise pour le fichier sélectionné;
    * une fonction _ecrit_balise_APPO_, qui effectue la même chose, mais pour la balise APPO;
    * une fonction _ecrit_DQT_, qui permet, grâce à un tableau de uint8_t, à la taille de la table et à l'indice considérén d'écrire une table de quantification pour le fichier considéré;
    * une fonction _ecrit_SOFO_, qui permet, grâce aux caractéristiques de fichier/image (nom, hauteur, largeur, nombre de couleurs, et tableau de quantification), qui permet d'écrire une balise SOFO;
    * une fonction _ecrit_DHT_, qui permet, avec le document, le type de table et l'indice de couleur, d'écrire une balise DHT;
    * une fonction _ecrit_SOS_, qui fonctionne de la même manière, sauf qu'on remplace le type de table par un tableau d'identifiants;
    * une fonction _ecrit_EOI_, qui utilise la fonction _ecrit_2octet_ sur le document passé en entrée;
    * une fonction _ecrit_octet_si_plein_, qui écrit l'octet si on est arrivé à saturation dans notre fichier;
    * une fonction _ecrit_code_, qui, à partir du fichier, de l'uint16_t code et de l'uint8_t nb_bt, écrit le code dans le fichier;
    * une fonction _ecrit_bitstream_, qui écrit l'octet si c'est possible dans le fichier (grâce aux paramètres entiers : code_huffman, nb_bit_h, indexe et nb_bit_index);
    * une fonction _force_ecriture_, qui force l'écriture d'un bitstream sur le fichier passé en entrée.
- **lecture** : contient :
    * une fonction _get_h1_, qui permet de renvoyer l'uint8_t correspondant au "h1" de l'argument saisi en entrée;
    * une fonction _get_h2_, qui fait la même chose, mais avec h2;
    * une fonction _get_h3_, qui fait la même chose, mais avec h3;
    * une fonction _get_v1_, qui fait la même chose, mais avec v1;
    * une fonction _get_v2_, qui fait la même chose, mais avec v2;
    * une fonction _get_v3_, qui fait la même chose, mais avec v3;
    * une fonction _get_origin_file_, qui renvoie le char correspondant au fichier d'origine de la structure Arguments saisie en entrée;
    * une fonction _get_outfile_, qui fait la même chose, mais avec le fichier de sortie;
    * une fonction _get_verbose_, qui renvoie le booléen correspondant à la verbose de la structure Arguments saisie en entrée;
    * une fonction _lecture_sample_, qui créé un tableau de uint8_t à partir d'une chaine de caractères (correspondant, dans notre programme, aux coefficients h1, h2, h3, v1, v2 et v3);
    * une fonction _free_arguments_, qui permet de libérer la mémoire de la structure arguments saisie en entrée;
    * une fonction _existe_, qui renvoie un booléen indiquant si le fichier d'entrée existe;
    * une fonction _affiche_aide_ (sans argument), qui permet d'afficher l'aide pour l'utilisateur, lui permettant d'exécuter le programme (avec, notamment, les différentes options à saisir);
    * une fonction _lecture_, qui prend, en entrée, un tableau de caractères et un entier argument, et qui renvoie une structure Arguments, qui dépend des éléments saisis par l'utilisateur;
- **MCU** : contient :
    * une fonction _creer_matrice_blocs_, qui créé une matrice de pointeurs de blocs de la largeur et de la hauteur saisie en entrée;
    * une fonction _mcu_, qui, à partir des uint8_t nb_colonnes_init, nb_lignes_init (indiquant le nombre de blocs initial en largeur/hauteur), h et v(coefficients d'échantillonage), et avec la matrice de blocs initiale, fournit une nouvelle matrice de pointeurs de blocs après application du sous-échantillonage.
- **mon_huffman** : contient :
    * une fonction _creer_codage_, qui renvoie un pointeur vers la structure Codage correspondant aux entiers valeur, code et nb_bits saisis en entrée;
    * une fonction _creer_table_, qui renvoie un pointeur vers la structure Table correspondant à la taille et au pointeur de la structure Codage saisie en entrée;
    * une fonction _free_table_, qui libère la mémoire de la structure Table saisie en entrée;
    * une fonction _maj_numero_, qui récupère l'entier suivant dans la table (dépend de l'entier actuel et du dernier bit);
    * une fonction _recup_table_, qui récupère les entiers dc_ac (0 pour DC, 1 pour AC) et y_cb_cr (0 pour Y, 1 pour Cb ou 2 pour Cr), dans le but de construire un pointeur vers une structure Table correspondant à ce qui a été saisi en entrée;
    * une fonction _huffman_, qui renvoie le coefficient codé avec Huffman via une structure Codage, à partir du coefficient d'entrée et du pointeur vers la table de Huffman considérée;
    * une fonction _affiche_codage_, qui affiche le nombre de bits sur lequel est codé ce qui est pointé par le pointeur vers la structure Codage d'entrée;
    * une fonction _affiche_table_, qui affiche les élément du pointeur vers la structure Table d'entrée.
- **quantification** : permet de donner l'uint16_t correspondant à la quantification d'un MCU, à partir des paramètres entiers indiquant la coordonnée lue, l'indice et le paramètre Y_CbCr.
- **zigzag** : contient :
    * deux fonctions _min_ et _max_, indiquant le plus petit/le plus grand des deux uint32_t saisis en entrée;
    * une fonction _zigzag_1_MCU_, qui renvoie un tableau de flottants correspondant au parcours en zigzag du MCU saisi en entrée.

## Structures de données du projet

| Nom de la structure | AC_RLE | Bloc | Pixel_YCbCr | Image | Arguments | Codage | Table |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------
| Composants | bool a_ecrire; uint8_t octet; uint8_t magnitude; uint8_t nb_balise_0 | uint32_t hauteur; uint32_t largeur; float **matrice | float Y; float Cr; float Cb | uint8_t nb_couleur; uint32_t nb_ligne; uint32_t nb_colonne | char *origin_file; char *outfile; bool verbose; uint8_t h1; uint8_t v1; uint8_t h2; uint8_t v2; uint8_t h3; uint8_t v3 | uint8_t valeur; uint16_t code; uint8_t nb_bits | uint16_t taille; struct Codage *tableau
| Informations | La structure AC_RLE nous indique les caractéristiques du codage RLE d'un élément AC (nombre de zéros, etc.). | Cette structure renvoie une matrice de flottants correspondant à un bloc, ainsi que la largeur et la hauteur de ce bloc (en nombre de composantes). | Cette structure contient les coordonnées d’un pixel, au format Y Cb Cr (chaque flottant représentant une composante). | La structure image contient différentes informations : un tableau de pixels (par définition même d’une image), le nombre de lignes, le nombre de colonnes, et nb_couleur (valant 1 si l’image est en noir et blanc, et 3 sinon). | Cette structure permet de savoir ce qui a été saisi par l'utilisateur dans le terminal (coefficients d'échantillonage, fichier d'entrée, fichier de sortie, et booléen verbose). | La structure Codage comprend le code (uint16_t), la valeur correspondante, et le nombre de bits sur lequel elle est codée. | Cette structure contient la taille de la table de Huffman, ainsi qu'un tableau correspondant à la table de Huffman.

## Répartition des tâches au sein de l'équipe de développement

_**Planning Tiburce :**_

| Jour | Lundi 02 Mai | Mardi 03 Mai | Mercredi 04 Mai | Jeudi 05 Mai | Vendredi 06 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Compréhension générale du projet. | Suite et fin de l'écriture du module zigzag. Début de la quantification. | _Cours de sport._ | Codage de la fonction fournissant la magnitude d'un entier et, par conséquent, le codage DC. | Amélioration des structures (réduction de leur nombre pour en créer des plus "globales").
| Après-midi | Début de l'écriture du code pour le parcours d'une image via un chemin de type "zigzag". | Suite et fin de la quantification. | Mise en place de la compression, grâce au codage de Huffman. | Suite et fin du codage de Huffman. | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 09 Mai | Mardi 10 Mai | Mercredi 11 Mai | Jeudi 12 Mai | Vendredi 13 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Correction d'erreurs (plus ou moins importantes) dans la plupart des modules. | Amélioration du code pour le découpage de l'image en MCU. | Fin de la correction des erreurs et création de nouvelles structures, afin d'étudier les images composante par composante. | Amélioration générale des différents codes et simplification des différentes structures présentes. | Fin de l'écriture dans le fichier JPEG (pour le cas couleur). **APPARITION DE BIIIIIIG !!! Et de toutes les images en couleur.**
| Après-midi | Code pour le découpage de l'image en MCU. | Correction des erreurs d'écriture pour les images avec découpage par blocs (noir et blanc). | Amélioration générale des différents codes et simplification des différentes structures présentes. | Amélioration générale du code et écriture dans le fichier JPEG pour le cas "couleur". | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 16 Mai | Mardi 17 Mai | Mercredi 18 Mai | Jeudi 19 Mai | Vendredi 20 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Mise en place de l'échantillonage pour l'écriture dans le fichier JPEG. | Amélioration des différents codes, débugage mémoire et réflexion autour de "temps ou mémoire ?". | Amélioration de la DCT grâce à l'algorithme de Chen. | **Soutenance orale (09h15-10h00).** | _Projet terminé._
| Après-midi | Fin de l'échantillonage pour l'écriture dans le fichier JPEG. | Amélioration du module "decoupage_bloc". | Amélioration de la DCT et "propreté" du code. | _Projet terminé._ | _Projet terminé._

_**Planning Quentin :**_

| Jour | Lundi 02 Mai | Mardi 03 Mai | Mercredi 04 Mai | Jeudi 05 Mai | Vendredi 06 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Compréhension générale du projet. | Poursuite du module DCT, correction progressive des erreurs. | _Cours de sport._ | Correction des erreurs sur le codage RLE. | Gestion des #include (simplification, suppression des doublons, etc). Dépôt sur le git des différentes informations sur notre projet (modules, structures de données et répartition des tâches). | Écriture du code pour le codage RLE des coefficients DC.
| Après-midi | Début de l'écriture du code pour le calcul des DCT pour les différents pixels. | Fin de l'écriture du module DCT. | Écriture du code pour le codage RLE des coefficients DC. | Amélioration des modules DCT et AC_RLE pour les adapter à des images en couleur. Ajout d'informations pour les modules suivants (taille du tableau de pixels sortant de AC_RLE, notamment). | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 09 Mai | Mardi 10 Mai | Mercredi 11 Mai | Jeudi 12 Mai | Vendredi 13 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Début de la compression des MCU (sous-échantillonage).  | Fin de la compression des MCU et tests. | Codage sur la compression des MCU terminé. Adaptation aux autres modules. | Fin de la modification pour la compression MCU. Adaptation de la fonction DCT à nos nouvelles structures. | Amélioration de la DCT (temps d'exécution divisé par 7 en moyenne), grâce au stockage des différentes valeurs de cosinus calculées.
| Après-midi | Poursuite de la compression des MCU (sous-échantillonage). | Corrections d'erreurs sur la compression des MCU. | Poursuite de l'adaptation du codage MCU (au fur-et-à-mesure de la simplification des structures et des écritures dans le fichier JPEG). Modification des tests effectués. | Amélioration générale des différents modules implémentés. | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 16 Mai | Mardi 17 Mai | Mercredi 18 Mai | Jeudi 19 Mai | Vendredi 20 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Étude d'améliorations (en temps) pour la DCT. | Amélioration de la DCT via procédure récursive, finalement abandonnée au vu des temps d'exécution avec les matrices. | Amélioration de la DCT grâce à l'algorithme de Chen. | **Soutenance orale (09h15-10h00).** | _Projet terminé._
| Après-midi | Étude d'améliorations (en temps) pour la DCT, et intérêt de ces améliorations (passage aux complexes ? Récursivité ?) | Mise à jour du README et amélioration des différents modules (coding-style, etc.) | Amélioration de la DCT, fin du carnet de bord, mise à jour du README et "propreté" du code. | _Projet terminé._ | _Projet terminé._

_**Planning Luc :**_

| Jour | Lundi 02 Mai | Mardi 03 Mai | Mercredi 04 Mai | Jeudi 05 Mai | Vendredi 06 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Compréhension générale du projet. | Début de l'étude de la méthode pour l'écriture dans le flux JPEG. | _Cours de sport._ | Poursuite de l'écriture dans le flux JPEG. | Fin de l'écriture dans le flux JPEG.
| Après-midi | Conversion des pixels : format RGB -> format YCbCr. Écriture de la structure "Image". | Poursuite de l'écriture dans le flux JPEG. | Poursuite de l'écriture dans le flux JPEG. Premiers résultats en termes d'écriture en hexadécimal. | Suite de l'écriture dans le flux JPEG. | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 09 Mai | Mardi 10 Mai | Mercredi 11 Mai | Jeudi 12 Mai | Vendredi 13 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Correction d'erreurs (plus ou moins importantes) dans la plupart des modules. | Améliorations générales du code (sur la mémoire, notamment). | Fin de la correction des erreurs. **APPARITION DE GRAY !!! Et de toutes les images en noir et blanc.** | Poursuite de l'écriture dans le fichier JPEG pour le cas "couleur", avec réflexion autour de l'échantillonage. | Fin de l'écriture dans le fichier JPEG (pour le cas couleur). **APPARITION DE BIIIIIIG !!! Et de toutes les images en couleur.**
| Après-midi | Fin de la correction des erreurs. **APPARITION d'INVADER !!!** | Correction des erreurs d'écriture pour les images avec découpage par blocs (en noir et blanc). | Écriture dans le flux JPEG pour le cas "couleur" (sans échantillonage pour le moment). | Poursuite de l'écriture dans le cichier JPEG pour le cas "couleur". | _Après-midi "banalisé" (en remplacement du Jeudi)._

| Jour | Lundi 16 Mai | Mardi 17 Mai | Mercredi 18 Mai | Jeudi 19 Mai | Vendredi 20 Mai
| ------ | ------ | ------ | ------ | ------ | ------ 
| Matin | Mise en place de l'échantillonage pour l'écriture dans le fichier JPEG. | Amélioration des différents codes, débugage mémoire et réflexion autour de "temps ou mémoire ?". Simplification de la DCT. | Amélioration de la DCT grâce à l'algorithme de Chen. | **Soutenance orale (09h15-10h00).** | _Projet terminé._
| Après-midi | Fin de l'écriture dans le fichier JPEG. | Amélioration du module "decoupage_bloc". | Amélioration de la DCT grâce à l'algorithme de Chen. | _Projet terminé._ | _Projet terminé._ | 

# **NOTRE JOURNAL DE BORD**

## Lundi 02 Mai 2022

#### Matin (9h00-12h00) :
- Présentation orale du projet par les professeurs;
- Prise en main du sujet en groupe, lecture des différents éléments fournis et répartition des premières tâches.
#### Après-midi (13h30-18h00) : 
- Réalisation des premiers éléments pour l'exemple le plus simple : image petite, en noir et blanc;
- Création de structures (Image, Pixel_YCrCb), et module permettant la conversion de RGB vers YCbCr;
- Mise en place d'une fonction permettant l'affichage des différentes composantes pour les pixels;
- Début de l'implémentation du module DCT (Transformée en Cosinus Discrète), correction progressive des erreurs et mise en place de tests;
- Début de l'implémentation du module zigzag permettant de réordonancer nos valeurs;

## Mardi 03 Mai 2022

#### Matin (8h00-12h00) :
- Poursuite de l'implémentation du module DCT. Amélioration des tests et adaptation aux autres modules mis en place;
- Début de l'étude de la méthode pour l'écriture dans le flux JPEG;
- Fin du module zigzag;
- Début de la quantification : premières fonctions.
#### Après-midi (13h30-18h00) : 
- Fin du module DCT;
- Mise en place de tests et fin de la quantification;
- Poursuite de l'écriture dans le flux JPEG;
- Première partie de la compression, grâce au codage de Huffman;
- Fin de l'implémentation du module zigzag permettant de réordonancer nos valeurs;
- Réalisation du carnet de bord des deux premiers jours.

## Mercredi 04 Mai 2022

#### Matin (10h00-12h00) :
- Cours de sport :-)

#### Après-midi (13h15-18h00)
- Poursuite de la compression (codage de Huffman);
- Poursuite de l'écriture dans le flux JPEG. Premiers résultats en termes d'écriture en hexadécimal;
- Écriture du code pour le codage des coefficients AC, grâce à un codage de type RLE; il permet de gérer les répétitions de 0 (à adapter en fonction des modules précédents);
- Étude de l'encodage dans le flux;
- Réalisation du carnet de bord de l'après-midi.

## Jeudi 05 Mai 2022

#### Matin (8h15-11h45)
- Poursuite de l'écriture dans le flux JPEG;
- Poursuite de la compression (codage de Huffman);
- Suite de l'encodage dans le flux;
- Correction d'erreurs mineures pour les coefficients AC et adaptation aux modules précédents;
- Réalisation du carnet de bord de la matinée.

#### Après-midi (12h30-17h30)
- Ajout de la sortie "taille" pour les coefficients AC;
- Amélioration de la DCT pour différencier les cas "Noir et blanc" et "Couleur";
- Suite de l'écriture dans le flux JPEG;
- Suite et fin de l'encodage dans le flux;
- Mise en commum des différents modules;
- Correction progressive des bugs, simplification des structures;
- Réalisation du carnet de bord de l'après-midi.

## Vendredi 06 Mai 2022

#### Matin (8h00-12h30)
- Amélioration des structures : simplification de celles-ci (certaines ont été supprimées pour les remplacer par des structures plus générales);
- Réécriture de manière "plus propre" (.h VS .c, gestion des #include, gestion des structures);
- Fin de l'écriture dans le flux JPEG;
- Réalisation du carnet de bord de la matinée.

#### Après-midi
_Après-midi banalisé (en remplacement du Jeudi)._

## Lundi 09 Mai 2022

#### Matin (8h15-12h15)
- Suite (et presque fin) de la correction des erreurs pour la première image (_invader.jpeg_);
- Début de l'étude des MCU.

#### Après-midi (13h15-19h00)
- Fin de la correction des erreurs. **APPARITION D'INVADER !**
- Code pour le découpage d'une image en MCU;
- Code pour la compression d'un MCU (partie 'sous-échantillonage');
- Début de l'adaptation des codes à une version plus élaborée d'image (couleur notamment);
- Réalisation du carnet de bord de la journée.

## Mardi 10 Mai 2022

#### Matin (8h15-12h00)
- Fin de la compression d'un MCU;
- Amélioration des différents codes (free pour la mémoire, par exemple) : corrections à l'aide de valgrind. Améliorations diverses sur le code pour le découpage en MCU;
- Réalisation du carnet de bord de la matinée.

#### Après-midi (13h15-18h00)
- Corrections d'erreurs sur la compression d'un MCU. Le code semble fonctionner;
- Détection d'erreurs dans l'affichage de l'image _gris.pgm_ (seule la moitié de l'image s'affiche). Tentatives de débugage.

## Mercredi 11 Mai 2022

#### Matin (8h15-12h15)
- Réalisation de tests sur la compression d'un MCU, correction des dernières erreurs;
- Fin de la correction des erreurs d'écriture. **APPARITION DE GRAY !!! Et de toutes les images en noir et blanc.**
- Améliorations diverses (création, notamment, d'une nouvelle structure, nous permettant de traiter les images composante par composante);
- Réalisation du carnet de bord de la veille (après-midi) et de la matinée.

#### Après-midi (13h15-17h15)
- Améliorations en termes de structures et d'efficacité de nos différents modules, avec, notamment, une réelle adaptation à l'écriture dans le fichier JPEG;
- Adaptation de la compression MCU à ces nouvelles structures;
- Début de l'écriture dans le fichier JPEG, dans le cas "couleur".

## Jeudi 12 Mai 2022

#### Matin (8h15-12h30)
- Poursuite des améliorations pour le code (création, notamment, d'une structure n'ayant qu'une seule composante de l'image);
- Fin de l'adaptation pour la compression MCU;
- Adaptation de la fonction DCT à ces nouvelles structures (avec adaptation des tests précédents pour vérification du bon fonctionnement);
- Poursuite de l'écriture dans le fichier JPEG (dans le cas "couleur"), avec, notamment, une réflexion autour de l'échantillonage;
- Réalisation du carnet de bord de la veille (après-midi) et de la matinée.

#### Après-midi (13h00-16h30)
- Poursuite de l'écriture dans le fichier JPEG pour le cas "couleur". Correction progressive des différents bugs;
- Amélioration de certains éléments du code (simplification des modules).

## Vendredi 13 Mai 2022

#### Matin (8h00-14h00)

- Fin de l'écriture dans le fichier JPEG dans le cas couleur. **APPARITION DE BIIIIIIG !!! Et de toutes les images en couleur**
  (temps d'exécution assez long - plusieurs minutes pour biiiiiig.jpeg -, car échantillonage pas encore mis en place);
- Amélioration des performances de la DCT, par stockage des différentes valeurs de cosinus (qui étaient recalculées plusieurs fois pour rien). Temps d'exécution divisé par 7 en moyenne;

#### Après-midi
_Après-midi banalisé (en remplacement du Jeudi)._

## Lundi 16 Mai 2022

#### Matin (8h15-11h45)
- Réflexion autour d'améliorations possibles de la DCT (via, par exemple, une procédure récursive);
- Mise en place de l'échantillonage pour l'écriture dans le fichier JPEG.

#### Après-midi (12h30-18h30)
- Poursuite de l'étude d'améliorations pour la DCT (passage par les complexes ? Procédure récursive ?). Petite amélioration via le stockage des valeurs (on ne calcule plus les valeurs des cosinus, mais on les fournit directement au programme);
- Fin de l'échantillonage pour l'écriture dans le fichier JPEG.

## Mardi 17 Mai 2022

#### Matin (8h15-11h45)
- Simplification de la DCT, via l'utilisation de matrices. Abandon de la procédure récursive, au vu de la rapidité d'exécution dans le cas de la DCT;
- Amélioration des différents codes : simplification des structures (plus que 2 à ce stade) et adaptation des modules en conséquence;
- Réflexion sur différents procédés : nous avons deux versions qui marchent pour les blocs, mais laquelle privilégier : la plus rapide, mais qui utilise beaucoup de mémoire ? Ou la plus lente, qui en utilise très peu ?.

#### Après-midi (12h45-19h15)
- Tentative d'amélioration du découpage des blocs utilisant très peu de mémoire, afin de réduire le temps d'exécution (5 secondes VS 4 minutes sur _biiiiiig.ppm_...);
- Mise à jour du _README_ avec les différentes structures désormais en place;
- Optimisations diverses, amélioration du coding-style;
- Amélioration de la DCT : tentatives de nouvelles implémentations pour gagner en rapidité;
- Réalisation des carnets de bord de : Jeudi 12 après-midi, Vendredi 13, Lundi 16 et Mardi 17.

## Mercredi 18 Mai 2022

#### Matin (8h00-12h30)
- Amélioration de la DCT via, notamment, l'algorithme de Chen. Essais de différentes implémentations;
- Mise en forme générale d'un code "propre".

#### Après-midi (12h45-18h30)
- (Fin de l')amélioration de la DCT pour gain de temps;
- Fin de la mise en forme;
- Réalisation du carnet de bord de la journée;
- Mise à jour du README;
- Fin du projet.
