#include <codage_DC.h>
#include <decoupage_bloc.h>

int main(){
    struct Image *image = init_image(5,5,3);
    for (uint32_t i=0; i<image->nb_ligne; i++){
        for (uint32_t j=0; j<image->nb_colonne;j++){
            image->pixel[i][j].Y = i;
        }
    }
    affiche(image);

    uint32_t hauteur_bloc = 4;
    uint32_t longueur_bloc = 4;
    
    for (uint32_t i = 0; i < nouvelle_dim(image->nb_ligne, hauteur_bloc); i++){
        for (uint32_t j = 0; j < nouvelle_dim(image->nb_colonne, longueur_bloc); j++){
            struct Bloc *blocs = recuperation_bloc(image, hauteur_bloc, longueur_bloc, i, j, 0);
            affiche_bloc(blocs);
        }
    }
    free_image(image);
}
