#include <quantification.h>
#include <zigzag.h>
#include <stdio.h>

int main(){
    struct Image *image = init_image(8,8,3);
    for (uint32_t i=0; i<image->nb_ligne; i++){
        for (uint32_t j=0; j<image->nb_colonne;j++){
            image->pixel[i][j].Y = 10*(i+j);
        }
    }
    affiche(image);
    struct Pixel_YCbCr *sortie = zigzag_1_MCU(image);
    for (int i = 0; i < 64; i++){
        printf("sortie: %f, quantif: %u\n", sortie[i].Y, quantification(sortie[i].Y, i, 0));
    }
}
