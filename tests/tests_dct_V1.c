#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "dct_V1.h"

int main(void){
  struct Image *image = init_image(5,5);
        for (uint32_t i=0; i<image->nb_ligne; i++){
            for (uint32_t j=0; j<image->nb_colonne;j++){
                image->pixel[i][j].Y = 0;
        image->pixel[4][4].Y = 0;
        image->pixel[2][3].Y = 1;

        }
    }

    /* AFFICHAGE DE L'IMAGE */
    printf("Image OK\n");
    affiche(image);
    struct Pixel_YCbCr *sortie = zigzag_1_MCU(image);
    printf("zigzag OK\n");
    /* CONSTRUCTION DEGUEULASSE D'UNE LISTE AVEC UNE STRUCTURE D'IMAGE */
    struct Image *image_sortie = init_image(1,image->nb_colonne*image->nb_ligne);
    image_sortie->pixel = &sortie;
    image_sortie->nb_ligne = 1;
    image_sortie->nb_colonne = image->nb_colonne*image->nb_ligne;
    /* AFFICHAGE DE LA LISTE */
    affiche(image_sortie);
    struct Pixel_YCbCr *nouvelle_sortie = codage_en_AC(sortie);
    /* CONSTRUCTION DEGUEULASSE D'UNE LISTE AVEC UNE STRUCTURE D'IMAGE */
    struct Image *nouvelle_image_sortie = init_image(1,image->nb_colonne*image->nb_ligne);
    nouvelle_image_sortie->pixel = &nouvelle_sortie;
    nouvelle_image_sortie->nb_ligne = 1;
    nouvelle_image_sortie->nb_colonne = image->nb_colonne*image->nb_ligne;
    /* AFFICHAGE DE LA LISTE */
    affiche(nouvelle_image_sortie);
    free_image(image);
}
