#include <math.h>
#include <stdio.h>
#include <zigzag.h>
#include <ImageYCbCr.h>

int main(){
    struct Image *image = init_image(3,3,3);
    for (uint32_t i=0; i<image->nb_ligne; i++){
        for (uint32_t j=0; j<image->nb_colonne;j++){
            image->pixel[i][j].Y = i+j;
        }
    }
    printf("Image OK\n");
    affiche(image);
    struct Pixel_YCbCr *sortie = zigzag_1_MCU(image);
    printf("zigzag OK\n");
    struct Image *image_sortie = init_image(1,image->nb_colonne*image->nb_ligne, 3);
    image_sortie->pixel = &sortie;
    image_sortie->nb_ligne = 1;
    image_sortie->nb_colonne = image->nb_colonne*image->nb_ligne;

    affiche(image_sortie);
    free_image(image);
}