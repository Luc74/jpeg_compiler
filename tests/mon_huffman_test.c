#include <htables.h>
#include <stdio.h>
#include <mon_huffman.h>
#include <stdlib.h>

int main(){
    struct Table *table = recup_table(0,0);
    affiche_table(table);
    struct Codage code = huffman(10, table);
    printf("%u\n",code.code);
    printf("%u\n",code.nb_bits);
    free_table(table);
}
