#include <ImageYCbCr.h>
#include <dct.h>
#include <zigzag.h>
#include <AC_RLE.h>
#include <codage_DC.h>
#include <qtables.h>
#include <MCU.h>
#include <decoupage_MCU.h>

// int main(){
//     // en tete
//     FILE* doc = fopen("tt.jpeg", "w+");
//     ecrit_balise_SOI(doc);
//     ecrit_balise_APPO(doc);
//     ecrit_DQT(doc, quantification_table_Y, 64 ,0);
//     uint8_t echantillonage[] = {1, 1, 0};
//     ecrit_SOFO(doc, 8, 8, 8, 1, echantillonage);
//     struct table *tables = recup_table(0, 0);
//     ecrit_DHT(doc, 0, 0);
//     ecrit_DHT(doc, 1, 0);
//     uint8_t id[] = {1, 0, 0};
//     ecrit_SOS(doc, 1, id);
//     //donner

//     struct Image *invader_ppm = chargement_RGB_YCbCr("images/invader.pgm");
//     affiche(invader_ppm);
//     struct Image *invader_ppm_dct = modif_image(invader_ppm);
//     affiche(invader_ppm_dct);
//     struct Pixel_YCbCr *ligne_pixel = zigzag_1_MCU(invader_ppm_dct);
//     struct Pixel **tab_pix = quantification(ligne_pixel);
//     struct zigzag *code_AC = codage_en_AC(*tab_pix);
//     int codege_DC = codage_DC(*tab_pix);
//     // printf("%i\n", codege_DC);
//     // huffman
//     struct equiv_bitstream **DC = huffman(&codege_DC, 1, 0, 0);
//     // printf("%u\n", (*DC)->code);
//     struct equiv_bitstream **AC = huffman(code_AC->tableau_zigzag, code_AC->taille, 1, 0);
//     //concatene DC et AC
    

//     //balise de fin
//     ecrit_EOI(doc);
//     fclose(doc);
// }

// int main(void)
// {
//     struct Image *image_ex = init_image(20, 20, 3);
//     for (uint32_t i = 0; i<= 1; i++){
//             for (uint32_t j = 0; j<= 1; j++){
//                 float valeur = i*2+j;
//                 image_ex->pixel[i][j].Y = valeur + 24*(i/(j+1));
//                 image_ex->pixel[i][j].Cb = valeur+2;
//                 image_ex->pixel[i][j].Cr = valeur+4;
//             }
//     }
//     affiche(image_ex);
//     affiche(modif_image(image_ex));
//     return EXIT_SUCCESS;
// }

// void affiche_Cb(struct Image *images){
//     printf("dimention : (%u %u)\n", images->nb_ligne, images->nb_colonne);
//     for(uint32_t i = 0; i< images->nb_ligne; i++){
//         for(uint32_t j = 0; j< images->nb_colonne; j++){
//             printf("%f ", images->pixel[i][j].Cb);
//         }
//         printf("\n");
//     }
// }

// void affiche_Cb_double(struct Image **images){
//     printf("dimention : (%u %u)\n", images[0]->nb_ligne, images[0]->nb_colonne);
//     for(uint32_t i = 0; i< images[0]->nb_ligne; i++){
//         for(uint32_t j = 0; j< images[0]->nb_colonne; j++){
//             printf("%f ", images[0]->pixel[i][j].Cb);
//         }
//         printf("\n");
//     }
// }

// int main(void){
//     struct Image *image_ex_1 = init_image(8, 8, 3);
//     for (uint32_t i = 0; i<= 7; i++){
//             for (uint32_t j = 0; j<= 7; j++){
//                 float valeur = i*2+j;
//                 image_ex_1->pixel[i][j].Y = valeur + 24*(i/(j+1));
//                 image_ex_1->pixel[i][j].Cb = valeur+2;
//                 image_ex_1->pixel[i][j].Cr = valeur+4;
//             }
//     }

// struct Image *image_ex_2 = init_image(8, 8, 3);
//     for (uint32_t i = 0; i<= 7; i++){
//             for (uint32_t j = 0; j<= 7; j++){
//                 float valeur = i*2+j;
//                 image_ex_2->pixel[i][j].Y = valeur + 25*(i/(j+1));
//                 image_ex_2->pixel[i][j].Cb = valeur+3;
//                 image_ex_2->pixel[i][j].Cr = valeur+5;
//             }
//     }
//     printf("Images de base :\n");
//     affiche_Cb(image_ex_1);
//     affiche_Cb(image_ex_2);
//     struct Image **tab_blocs = {image_ex_1, image_ex_2};
//     affiche_Cb(tab_blocs[0]);
//     printf("AFFICHÉ\n");
//     printf("ICI\n");
//     uint8_t tab[] = {2,2,2};
//     struct Image *image_Cb = mcu_Cb(tab_blocs, tab, tab);
//     printf("Image Cb\n");
//     affiche_Cb_double(image_Cb);
//     printf("PLUS DE FAULT !\n");
//     struct Image *image_Cr = mcu_Cr(tab_blocs, tab, tab);
//     affiche(image_Cr);
// }

int main(){
    struct Image *tab_blocs = init_image(4,10,10);
    for (uint32_t i=0; i<tab_blocs->nb_ligne; i++){
        for (uint32_t j=0; j<tab_blocs->nb_colonne;j++){
            tab_blocs->pixel[i][j].Y = i;
        }
    }
    printf("YOYO\n");
    affiche(tab_blocs);
    uint32_t hauteur_bloc = 3;
    uint32_t longueur_bloc = 3;
    uint32_t nb_blocs = ceilf((float) tab_blocs->nb_ligne/hauteur_bloc) * ceilf((float)tab_blocs->nb_colonne/longueur_bloc);
    printf("TAILLE %u\n", nb_blocs);
    uint8_t tab_h_colonnes[] = {4,2,2};
    uint8_t tab_v_lignes[] = {2,1,1};
    uint8_t nb_mcu_Cb = tab_h_colonnes[2]*tab_v_lignes[2];
    struct Image **blocs = mcu_Cb(recuperation_MCU(tab_blocs, longueur_bloc, hauteur_bloc), tab_h_colonnes, tab_v_lignes);
    printf("J'AI COMPRIS !\n");
    for (uint32_t i = 0; i < nb_mcu_Cb; i++){
        printf("bloc numéro %u:\n", i);
        affiche(blocs[i]);
    }
}