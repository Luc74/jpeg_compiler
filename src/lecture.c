#include <lecture.h>

struct Arguments{
    char *origin_file;
    char *outfile;
    bool verbose;
    uint8_t h1;
    uint8_t v1;
    uint8_t h2;
    uint8_t v2;
    uint8_t h3;
    uint8_t v3;
};

/*
Récupère h1
*/
uint8_t get_h1(struct Arguments *arg){
    return arg->h1;
}

/*
Récupère v1
*/
uint8_t get_v1(struct Arguments *arg){
    return arg->v1;
}

/*
Récupère h2
*/
uint8_t get_h2(struct Arguments *arg){
    return arg->h2;
}

/*
Récupère h3
*/
uint8_t get_h3(struct Arguments *arg){
    return arg->h3;
}

/*
Récupère v2
*/
uint8_t get_v2(struct Arguments *arg){
    return arg->v2;
}

/*
Récupère v3
*/
uint8_t get_v3(struct Arguments *arg){
    return arg->v3;
}

/*
Récupère origin_file
*/
char *get_origin_file(struct Arguments *arg){
    return arg->origin_file;
}

/*
Récupère outfile
*/
char  *get_outfile(struct Arguments *arg){
    return arg->outfile;
}

/*
Récupère verbose
*/
bool get_verbose(struct Arguments *arg){
    return arg->verbose;
}

/*
CREE un tableau d'entiers (dans l'ordre: h1, v1, h2, v2, h3, v3)
*/
 static uint8_t *lecture_sample(char *chaine){

    // Initialisation:
    uint8_t *tab = calloc(sizeof(uint8_t), 6);
    int i = 0;
    uint8_t taille = 0;
    uint8_t valeur;
    char sep;


    // Lecture et remplissage du tableau:
    while(chaine[i] != '\0'){

        // Gestion des séparateurs des nombres (x ou , une fois sur deux)
        switch(taille%2){
            case 0:
                sep = 'x';
                break;
            case 1:
                sep = ',';
        }

        // Lecture d'un nombre:
        valeur = 0;
        while (chaine[i] != sep && chaine[i] != '\0'){

            // Si on a un caractère non attendu:
            if (chaine[i] < 48 || chaine[i] > 57){
                fprintf(stderr, "argument non reconnu (essayez la forme --sample=h1xv1,h2xv2,h3xv3)\n");
                return NULL;
            }
            valeur = 10*valeur + chaine[i]-48;
            i++;
        }
        tab[taille] = valeur;
        taille++;
        if (chaine[i] != '\0'){
            i++;
        }
    }

    // Si on a pas exactement 6 coefficients:
    if (taille != 6){
        fprintf(stderr, "argument non reconnu (essayez la forme --sample=h1xv1,h2xv2,h3xv3)\n");
        return NULL;
    }

    return tab;
}

/*
Libère la structure arguments
*/
void free_arguments(struct Arguments *arg){
    if (arg != NULL){
        if (arg->outfile != NULL){
            free(arg->outfile);
        }
        if (arg->origin_file != NULL){
            free(arg->origin_file);
        }
        free(arg);
    }
}

/*
Fonction vérifiant si le fichier existe
*/
static bool existe(char *fichier){
    FILE *file = fopen(fichier, "r");
    if (file == NULL){
        return false;
    }
    fclose(file);
    return true;
}

/*
Affiche l'aide
*/
static void afficher_aide(){
    printf("\nUsage: ppm2jpeg [OPTIONS...] [FICHIER]\n\n");
    printf("[FICHIER]\n\n     paramètre obligatoire permettant de définir l'image à compresser (en chemin relatif ou absolu). Cette image doit être au format .pgm ou .ppm\n");
    printf("\n");
    printf("[OPTIONS...]\n\n");
    printf("    -v, --verbose                   permet de voir ce que le programme écrit dans l'image jpeg lors de la compression.\n");
    printf("    -o, --outfile=fichier           définit le fichier de sortie.\n");
    printf("    -s, --sample=h1xv1,h2xv2,h3xv3  permet de définir les valeurs nécessaires au sous-échantillonage (pour les images en couleur)\n");
    printf("        --help                      affiche ce manuel d'aide.\n\n");
}

/*
Récupère les arguments en entrée
*/
struct Arguments *lecture(char *argv[], int argc){

    // Initilisation:
    struct Arguments *arguments = malloc(sizeof(struct Arguments));
    arguments->verbose = false;
    arguments->h1 = 1;
    arguments->v1 = 1;
    arguments->h2 = 1;
    arguments->v2 = 1;
    arguments->h3 = 1;
    arguments->v3 = 1;
    arguments->outfile = NULL;
    arguments->origin_file = NULL;
    uint8_t *tab;

    const char *optstring = ":vo:s:";

    static struct option options_longues[] = 
    {
        {"help", no_argument, NULL, 'h'},
        {"verbose", no_argument, NULL, 'v'},
        {"outfile", required_argument, NULL, 'o'},
        {"sample", required_argument, NULL, 's'},
        {0, 0, 0, 0}
    };

    // Gestion des arguments:
    int opt = getopt_long(argc, argv, optstring, options_longues, NULL);
    while(opt != -1){
        switch(opt){

            // Aide:
            case 'h':
                afficher_aide();
                free(arguments);
                return NULL;

            // Verbose:
            case 'v':
                arguments->verbose = true;
                break;

            // Outfile:
            case 'o':
                arguments->outfile = malloc(sizeof(optarg));
                strcpy(arguments->outfile, optarg);
                break;

            // Sample:
            case 's':
                tab = lecture_sample(optarg);
                arguments->h1 = tab[0];
                arguments->v1 = tab[1];
                arguments->h2 = tab[2];
                arguments->v2 = tab[3];
                arguments->h3 = tab[4];
                arguments->v3 = tab[5];
                free(tab);
                break;

            // Argument non valide:
            case '?':
                printf("Option non reconnue: %c\n", optopt);
                break;
        }
        opt = getopt_long(argc, argv, optstring, options_longues, NULL);
    }

    // Récupération du fichier source:
    if (optind == argc - 1){

        // Récupération du chemin:
        arguments->origin_file = malloc(strlen(argv[optind])+1);
        strcpy(arguments->origin_file, argv[optind]);

        // Vérification de l'existance du fichier:
        if (!existe(arguments->origin_file)){
            printf("Fichier source non existant\n");
            free(arguments->origin_file);
            free(arguments);
            return NULL;
        }

        // Attribution d'un nom par défaut au fichier de sortie
        // si --outfile n'est pas dans les options:
        if (arguments->outfile == NULL){

            // On va chercher le nom de
            uint8_t indice = 0;
            while (arguments->origin_file[indice] != '.' && arguments->origin_file[indice] != '\0'){
                indice++;
            }
            arguments->outfile = malloc(indice + 5);
            for (uint32_t i=0; i < indice; i++){
                arguments->outfile[i] = arguments->origin_file[i];
            }
            arguments->outfile[indice] = '\0';
            strcat(arguments->outfile, ".jpg");
        }
    }

    // Si le fichier source n'est pas indiqué:
    else{
        printf("Il manque le fichier source (ou il y a trop d'arguments)\n");
        free(arguments);
        return NULL;
    }
    return arguments;
}
