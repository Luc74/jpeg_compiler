#include <codage_DC.h>

/*
Fournit la classe de magnitude du numéro donné en argument.
*/
uint8_t magnitude(int numero){
    if (numero==0){
        return 0;
    }
    return (uint8_t) (log2(abs(numero)))+1;
}


/*
Renvoie la composante DC encodée d'un bloc 8x8 à partir du vecteur contenant les composantes du bloc.
*/
uint8_t codage_DC(int32_t pixel, int32_t ancien_dc){
    return magnitude(pixel - ancien_dc);
}

/*
Renvoie l'index de coef dans la classe de magnitude magnitude
*/
uint16_t indexe(int16_t coef, uint8_t magnitude){
    if (coef >= 0){
        return (uint16_t) coef;
    }
    return (coef+pow(2, magnitude)-1);
}
