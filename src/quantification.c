#include <quantification.h>


/*
La fonction qui se charge de la quantification d'un MCU lu en zigzag.
Le paramètre Y_CbCr vaut 0 si on lit une coordonnée Y, 1 our Cb et 2 pour Cr.
Remarque: on arrondit à l'entier le plus proche de 0 pour optimiser la compression
*/
int16_t quantification(float param, uint8_t indice, uint8_t Y_CbCr){
    uint8_t *quantif_table;

    switch(Y_CbCr){
        case 0:
            quantif_table = quantification_table_Y;
            break;
        case 1:
        case 2:
            quantif_table = quantification_table_CbCr;
    }
    if (param > 0){
        return (int16_t) (param/quantif_table[indice]);
    }
    return (int16_t) ceilf(param/quantif_table[indice]);
}
