#include <AC_RLE.h>



/* 
crée et renvoie une struct AC_RLE indiquant si on doit écrire son contenu et si c'est le cas le code a écrire.
Si on ne doit pas l'écrire, son code contient le nombre de zéros lus.
 */
struct AC_RLE *codage_en_AC(int16_t pixel,
                            uint8_t nb_zero,
                            uint8_t nb_balises_0,
                            bool fin_bloc)
{
    struct AC_RLE *code = malloc(sizeof(struct AC_RLE)); 
    
    if(pixel == 0){
        nb_zero++;
        //cas en fin de bloc
        if(fin_bloc){
            code->octet = 0;
            code->a_ecrire = true;
            code->nb_balises_0 = 0;
            code->magnitude = 0;
            return code;
        }
        //cas à 16 zéros
        if(nb_zero<16){
            code->a_ecrire = false;
            code->octet = nb_zero;
            code->magnitude = 0;
            code->nb_balises_0 = nb_balises_0;
            return code;
        }
        //on a 16 zéros et on attend de savoir si on
        //a un autre élément non nul avant la fin du bloc
        code->a_ecrire = false;
        code->octet = 0;
        code->magnitude = 0;
        code->nb_balises_0 = nb_balises_0 + 1;
        return code;
    }
    //on a pas de zéros, on peut écrire

    code->a_ecrire = true;
    //on calcule la classe de magnitude
    uint8_t magnetude = magnitude((int) pixel);
    code->magnitude = magnetude;
    //on décale de 4 notre magnitude
    code->octet = (nb_zero << 4) + magnetude;
    code->nb_balises_0 = nb_balises_0;
    return code;
}
