#include <decoupage_bloc.h>

/*
Initialise un bloc vide
*/
struct Bloc *creer_bloc(uint32_t hauteur, uint32_t longueur){
    struct Bloc *bloc = malloc(sizeof(struct Bloc));
    bloc->hauteur = hauteur;
    bloc->largeur = longueur;
    bloc->matrice = calloc(sizeof(float *), hauteur);
    for (uint32_t i = 0; i < hauteur; i++){
        bloc->matrice[i] = calloc(sizeof(float), longueur);
    }
    return bloc;
}

/*
Affiche un Bloc
*/
void affiche_bloc(struct Bloc *bloc){
    printf("Taille du bloc: %u, %u\n", bloc->hauteur, bloc->largeur);
    for (uint32_t i = 0; i < bloc->hauteur; i++){
        for (uint32_t j = 0; j < bloc->largeur; j++){
            printf("%f ", bloc->matrice[i][j]);
        }
        printf("\n");
    }
}

/*
Libération de mémoire pour un Bloc
*/
void free_bloc(struct Bloc *bloc){
    for (uint32_t i = 0; i < bloc->hauteur; i++){
        free(bloc->matrice[i]);
    }
    free(bloc->matrice);
    free(bloc);
}

/*
Renvoie la dimension (en blocs et pour 1D) de l'image issue de l'agrandissement
d'une image de dimension dim_init pour des blocs de dimension dim_blocs et de taille taille_bloc
*/
uint32_t nouvelle_dim(uint32_t dim_init, uint32_t dim_bloc){
    return ceilf((float) dim_init/dim_bloc);
}

/*
Renvoie la coordonnée d'un pixel (dans l'image de dimension dim_init) 
ayant les mêmes caractéristiques que le pixel d'indice indice dans l'image étendue.
*/
uint32_t extension(uint32_t dim_init,
                        uint32_t indice)
                        {
    if (indice > dim_init){
        return dim_init - 1;
    }
    return indice;
}

/*
Une fonction qui récupère les blocs depuis l'image originale
*/
struct Bloc *recuperation_bloc(struct Image *image_originale,
                                uint32_t hauteur_bloc,
                                uint32_t longueur_bloc,
                                uint32_t i_bloc,
                                uint32_t j_bloc,
                                uint8_t Y_Cb_Cr
                                ){

    //Initialisation:
    struct Bloc *bloc = creer_bloc(hauteur_bloc, longueur_bloc);

    //Ecriture:
    for (uint32_t i = 0; i < hauteur_bloc; i++){
        for (uint32_t j = 0; j < longueur_bloc; j++){

            uint32_t ligne = i_bloc * hauteur_bloc + i;
            uint32_t colonne = j_bloc * longueur_bloc + j;

            //Recopie:
            if (ligne >= image_originale->nb_ligne){
                ligne = image_originale->nb_ligne -1;
            }
            if (colonne >= image_originale->nb_colonne){
                colonne = image_originale->nb_colonne -1;
            }
            switch (Y_Cb_Cr){
                case 0:
                    bloc->matrice[i][j] = image_originale->pixel[ligne][colonne].Y;
                    break;
                case 1:
                    bloc->matrice[i][j] = image_originale->pixel[ligne][colonne].Cb;
                    break;
                case 2:
                    bloc->matrice[i][j] = image_originale->pixel[ligne][colonne].Cr;
            }
        }
    }
    return bloc;
}

