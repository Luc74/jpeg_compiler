#include <dct.h>
#include <assert.h>

#ifndef M_PI
#define M_PI 3.1415926536
#endif 

/*
Fonction permettant de libérer la mémoire associée au tableau créé ci-dessus:
*/
void free_tableau(float **tab, uint8_t taille_bloc){
    for (uint8_t i = 0; i < taille_bloc; i++){
        free(tab[i]);
    }
    free(tab);
}

/*récipère le tableau de constante demander par la dct */
double *recup_vet_cons_bloc8x8(){
    double *cons = calloc(7, sizeof(double));
    cons[0] = cos(M_PI/4);
    cons[1] = cos(M_PI/8);
    cons[2] = sin(M_PI/8);
    cons[3] = cos(3*M_PI/16);
    cons[4] = sin(3*M_PI/16);
    cons[5] = cos(M_PI/16);
    cons[6] = sin(M_PI/16);
    return cons;
}

/*calcule DCT 1D par loffler */
static void traitement_loffer_8x8(struct Bloc *resultat, struct Bloc *donner, bool prem, double *constante){
    double var_1[8];
    double var_2[8];
    for(uint8_t i = 0; i<8; i++){
        //init offler
        var_1[0] = donner->matrice[i][0] + donner->matrice[i][7];
        var_1[1] = donner->matrice[i][1] + donner->matrice[i][6];
        var_1[2] = donner->matrice[i][2] + donner->matrice[i][5];
        var_1[3] = donner->matrice[i][3] + donner->matrice[i][4];
        var_1[4] = donner->matrice[i][3] - donner->matrice[i][4];
        var_1[5] = donner->matrice[i][2] - donner->matrice[i][5];
        var_1[6] = donner->matrice[i][1] - donner->matrice[i][6];
        var_1[7] = donner->matrice[i][0] - donner->matrice[i][7];
        if(prem){
            var_1[0] -= 2*128;
            var_1[1] -= 2*128;
            var_1[2] -= 2*128;
            var_1[3] -= 2*128;
        }
        //premiere etage
        var_2[0] = var_1[0] + var_1[3];
        var_2[1] = var_1[1] + var_1[2];
        var_2[2] = var_1[1] - var_1[2];
        var_2[3] = var_1[0] - var_1[3];
        var_2[4] = constante[6]*var_1[4]+constante[5]*var_1[7];
        var_2[5] = constante[4]*var_1[5]+constante[3]*var_1[6];
        var_2[6] = constante[4]*var_1[6]-constante[3]*var_1[5];
        var_2[7] = constante[6]*var_1[7]-constante[5]*var_1[4];
        //deusième etage
        var_1[0] = constante[0]*(var_2[0] + var_2[1]);
        var_1[1] = constante[0]*(var_2[0]-var_2[1]);
        var_1[2] = constante[2]*var_2[2]+constante[1]*var_2[3];
        var_1[3] = constante[2]*var_2[3]-constante[1]*var_2[2];
        var_1[4] = var_2[4] + var_2[5];
        var_1[5] = var_2[4] - var_2[5];
        var_1[6] = var_2[6] + var_2[7];
        var_1[7] = var_2[7] - var_2[6];
        //derniere etage et reogranisation
        resultat->matrice[i][0] = var_1[0];
        resultat->matrice[i][4] = var_1[1];
        resultat->matrice[i][2] = var_1[2];
        resultat->matrice[i][6] = var_1[3];
        resultat->matrice[i][1] = var_1[4];
        resultat->matrice[i][5] = constante[0]*(var_1[5]-var_1[6]);
        resultat->matrice[i][3] = constante[0]*(var_1[5]+var_1[6]);
        resultat->matrice[i][7] = var_1[7];
    }
};

/*transpose la matrisse d'une structure bloc */
static void transpose(struct Bloc *bloc){
    float **tab = calloc(8, sizeof(float *));
    for(uint8_t i = 0; i<8; i++){
        tab[i] = calloc(8, sizeof(float));
        for(uint8_t j = 0; j<8; j++){
            tab[i][j] = bloc->matrice[j][i];
        }
    }
    free_tableau(bloc->matrice, 8);
    bloc->matrice = tab;
}


/* calcule la dct */
struct Bloc *dct_8x8(double *constante, struct Bloc *bloc){
    assert(bloc->hauteur == 8);
    assert(bloc->largeur == 8);
    struct Bloc *result = creer_bloc(8, 8);
    traitement_loffer_8x8(result, bloc, true, constante);
    transpose(result);
    traitement_loffer_8x8(result, result, false, constante);
    transpose(result);
    //on multiplie par 1/2 a cause des coefficient de noramilsation non present
    //on normalise que maintenant pour garder des nombre plus grend et limiter les 
    //impressision
    for(uint8_t i = 0; i<8; i++){
        for(uint8_t j = 0; j<8; j++){
            result->matrice[i][j] /= 4;
        }
    }
    return result;
}