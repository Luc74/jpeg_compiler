#include <MCU.h>

/*
Permet de créer une matrice de pointeurs de blocs, avec la largeur et la hauteur saisis en entrée.
*/

static struct Bloc ***creer_matrice_blocs(uint8_t largeur, uint8_t hauteur){
    struct Bloc ***matrice_blocs = calloc(hauteur, sizeof(struct Bloc**));
    for (uint8_t i = 0; i < hauteur; i++){
        matrice_blocs[i] = calloc(largeur, sizeof(struct Bloc*));
        for(uint8_t j = 0; j<largeur; j++){
            matrice_blocs[i][j] = creer_bloc(8, 8);
        }

    }
    return matrice_blocs;
}

/*
Prend, en entrée, un tableau de MCU (image).
Crée un tableau contenant les blocs qui contiendront une composante Cb.
h et v représentent, respectivement, les "colonnes" et "les lignes".
*/

struct Bloc ***mcu(uint8_t nb_colonnes_init, 
                   uint8_t nb_lignes_init,
                   struct Bloc *blocs_init[nb_lignes_init][nb_colonnes_init],
                   uint8_t h, uint8_t v)
{
    // Initialisation:
    struct Bloc ***blocs_mcu = creer_matrice_blocs(h, v);
    uint8_t ratio_lignes = nb_lignes_init/v;
    uint8_t ratio_colonnes = nb_colonnes_init/h;
    float moyenne_a_ajouter = 0;

    for (uint8_t i = 0; i < v; i++){ /* On remplit tous les MCU : il y en a v x h */
        for (uint8_t j = 0; j<h; j++){
            for (uint8_t ligne_copiee = 0; ligne_copiee<8; ligne_copiee++){ /* Remplissage de chaque composante du MCU considéré */
                for (uint8_t colonne_copiee = 0; colonne_copiee<8; colonne_copiee++){

                    // Récupération des indices pour faire la moyenne:
                    uint8_t indice_ligne = (ligne_copiee*ratio_lignes)%8; /* Première ligne à étudier du MCU originel */
                    uint8_t indice_colonne = (colonne_copiee*ratio_colonnes)%8; /* Première colonne à étudier du MCU originel */
                    uint8_t indice_ligne_mcu = (ratio_lignes*i)+(ligne_copiee*ratio_lignes)/8;
                    uint8_t indice_colonne_mcu = (ratio_colonnes*j)+(colonne_copiee*ratio_colonnes)/8;
                    uint8_t prochain_indice_ligne = ((ligne_copiee + 1)*ratio_lignes)%8;
                    uint8_t prochain_indice_colonne = ((colonne_copiee + 1)*ratio_colonnes)%8;

                    /* Cas où on arrive à la fin de la ligne : on a alors 0 pour la prochaine ligne, ce qui pose problème pour le for suivant ! */
                    if (prochain_indice_ligne < indice_ligne){ 
                        prochain_indice_ligne = 8;
                    }
                    if (prochain_indice_colonne < indice_colonne){ /* Idem, mais pour la fin de colonne. */
                        prochain_indice_colonne = 8;
                    }

                    // Calcul de la moyenne:
                    for (uint8_t ligne = indice_ligne; ligne < prochain_indice_ligne; ligne++){
                        for (uint8_t colonne = indice_colonne; colonne < prochain_indice_colonne; colonne++){
                            moyenne_a_ajouter += blocs_init[indice_ligne_mcu][indice_colonne_mcu]->matrice[ligne][colonne];
                        }
                    }

                    // Ecriture de la moyenne dans le nouveau bloc:
                    moyenne_a_ajouter /= (prochain_indice_ligne-indice_ligne)*(prochain_indice_colonne-indice_colonne);
                    blocs_mcu[i][j]->matrice[ligne_copiee][colonne_copiee] = moyenne_a_ajouter;
                    moyenne_a_ajouter = 0;
                }
            }
        }
    }
    return blocs_mcu;
}
