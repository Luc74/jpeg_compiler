#include <ImageYCbCr.h>

/* Octet en cours d'écriture */
static unsigned char octet_prepa = 0;
static uint8_t occup = 0;

/* créé une structure d'image*/
struct Image *init_image(uint32_t nb_ligne, uint32_t nb_colonne, uint8_t nb_couleure){
    struct Image *image = malloc(sizeof(struct Image));
    image->nb_colonne = nb_colonne;
    image->nb_ligne = nb_ligne;
    image->nb_couleur = nb_couleure;
    image->pixel = calloc(nb_ligne, sizeof(struct Pixel_YCbCr*));
    for(uint32_t i = 0; i<nb_ligne; i++){
        image->pixel[i] = calloc(nb_colonne, sizeof(struct Pixel_YCbCr));
    }
    return image;
}

/*affiche une structure d'image */
void affiche(struct Image *images){
    printf("dimention : (%u %u)\n", images->nb_ligne, images->nb_colonne);
    for(uint8_t c = 0; c<images->nb_couleur; c++){
        printf("_________couleur %u_________\n \n",c);
        for(uint32_t i = 0; i< images->nb_ligne; i++){
            for(uint32_t j = 0; j< images->nb_colonne; j++){
                printf("%f ", images->pixel[i][j].Y);
            }
            printf("\n");
        }
    }
}

/*libère la mémoire associée à l'image */
void free_image(struct Image *image){
    /*On libère nos colonnes */
    for(uint32_t i = 0; i<image->nb_ligne; i++){
        // on libère nos pixels
        free(image->pixel[i]);
    }
    //on libère nos lignes
    free(image->pixel);
    //on libère notre image
    free(image);
}


/*détecte si on est en little Endian ou non : vient de https://openclassrooms.com/forum/sujet/little-endian-ou-big-endian */
static int IsLittleEndian(){
    //on code 1 pour avoir le dernier bit a 1 et les autres a 0
    short s = 1;
    //on collecte le premier bit codé dans la machine
    //donc 1 si little endian
    //0 sinon
    char* c = (char*)&s;
    //on renvoie ce bit
    return *c;
}

/* écrit un char 16 indépendamment du processeur*/
static void ecrit_2octet(char16_t double_octet, FILE *doc){
    if(IsLittleEndian() == 1){double_octet = ntohs(double_octet);}
    fwrite(&double_octet, 1, sizeof(double_octet), doc);
}

/* ecrit un char 32 indépendamment du processeur*/
static void ecrit_4octet(char32_t double_octet, FILE *doc){
    if(IsLittleEndian() == 1){double_octet = ntohl(double_octet);}
    fwrite(&double_octet, 1, sizeof(double_octet), doc);
}

/* écrit une balise SOI et vide le fichier d'écriture s'il n'était pas vide*/
void ecrit_balise_SOI(FILE* doc){
    char16_t octet = 0xffd8;
    ecrit_2octet(octet, doc);
}

/*écrit la balise APPO dans le fichier doc*/
void ecrit_balise_APPO(FILE* doc){
    //écriture de la balise
    ecrit_2octet(0xffe0, doc);
    //on écrit la taille
    ecrit_2octet(0x0010, doc);
    // jifi
    ecrit_4octet(0x4a464946, doc);
    // \0 est le premier octet à 1
    ecrit_2octet(0x0001, doc);
    //écriture de la fin
    ecrit_4octet(0x01000000, doc);
    ecrit_4octet(0x00000000, doc);
}

/*écriture d'une table de quantification */
void ecrit_DQT(FILE* doc,
               uint8_t* qtables,
               uint8_t taille_table,
               uint8_t indice){
    //on écrit la balise
    ecrit_2octet(0xffdb, doc);
    //on écrit pour réserver les deux octets de longeur longeur
    char16_t taille = taille_table + 0x2+0x1;
    ecrit_2octet(taille, doc);
    // on écrit la précision et l'indice
    // on attend un uint8_t et on a l'indice qui ne déborde pas sur les bits de poids fort,
    //donc on peut se contenter de les additionner
    unsigned char octet = indice;
    fwrite(&octet, 1, sizeof(octet), doc);
    //on écrit notre table
    for(uint8_t i = 0; i<taille_table; i++){
        fwrite(&qtables[i], 1, sizeof(qtables[i]), doc);
    }

}

/* écriture d'une balise SOFO */
void ecrit_SOFO(FILE* doc,
                uint8_t precision,
                uint32_t hauteur, 
                uint32_t largeur,
                uint8_t nb_couleur,
                uint8_t *facteur_echantillonage_DQT){
    //on écrit le marqueur
    ecrit_2octet(0xffc0, doc);
    //on écrit la taille
    char16_t taille = 3*nb_couleur + 0x8;
    ecrit_2octet(taille, doc);
    //on écrit la précision
    fwrite(&precision, 1, sizeof(precision), doc);
    //on écrit la taille
    ecrit_2octet(hauteur, doc);
    ecrit_2octet(largeur, doc);
    //le nombre de composantes
    fwrite(&nb_couleur, 1, sizeof(nb_couleur), doc);
    //on écrit pour les composantes
    for(uint8_t i = 0; i<nb_couleur*3; i+=3){
        //on écrit l'identifiant Y 1, Cb 2, Cr 3
        unsigned char id = i/3 +0x1;
        fwrite(&id, 1, sizeof(id), doc);
        //on écrit nos facteurs d'échantillonage
        unsigned char echantillonage = (facteur_echantillonage_DQT[i]<<4)+//on décale pour que ce soit nos bits de poids fort
                                       facteur_echantillonage_DQT[i+1];
        fwrite(&echantillonage, 1, sizeof(echantillonage), doc);
        //on écrit notre indice de quantification
        fwrite(&facteur_echantillonage_DQT[i+2], 1, sizeof(facteur_echantillonage_DQT[i+3]), doc);
    }
}

/* écriture d'une balise DHT*/
void ecrit_DHT(FILE *doc,
               uint8_t type_table,
               uint8_t indice_couleur){
    //on écrit la balise
    ecrit_2octet(0xffc4, doc);
    // on écrit un taille arbitraire que l'on mettra à jour
    ecrit_2octet(0xffff, doc);
    //on écrit l'indice
    unsigned char indice = (type_table<<4)+//on consrtuit nos bits de poids fort
                           indice_couleur; //0 ou 1 en baseline
    fwrite(&indice, 1, sizeof(indice), doc);
    uint8_t *nb_symbol = htables_nb_symb_per_lengths[type_table][indice_couleur];
    for(uint8_t i = 0; i<16; i++){
        fwrite(&nb_symbol[i], 1, sizeof(nb_symbol[i]), doc);
    }
    // on collecte la taille de l'élément à venir pour mettre à
    //jour notre taille d'en-tête
    uint16_t taille = 19;
    //on écrit la table des symboles triés par longueur
    uint8_t *symbole_lg = htables_symbols[type_table][indice_couleur];
    uint8_t taille_table = htables_nb_symbols[type_table][indice_couleur];
    for(uint8_t i = 0; i<taille_table; i++){
        fwrite(&symbole_lg[i], 1, sizeof(symbole_lg[i]), doc);
        taille ++;
    }
    //on met à jour notre taille
    fseek(doc, -taille, SEEK_CUR);
    ecrit_2octet(taille, doc);
    //on remet le curseur à la fin
    // -2 car on a écrit deux octets
    fseek(doc, taille-2, SEEK_CUR);
}

/* écriture d'une balise SOS*/
void ecrit_SOS(FILE *doc, uint8_t nb_couleur, uint8_t *identifiant){
    //on écrit le marqueur
    ecrit_2octet(0xffda, doc);
    //on écrit la taille que l'on connait
    ecrit_2octet(2*nb_couleur+6, doc);
    //on écrit le nombre de composante
    fwrite(&nb_couleur, 1, sizeof(nb_couleur), doc);
    //on écrit les caractéristiques des composantes
    //donc notre tableau identifiant contient N fois 3 informations
    for(uint8_t i = 0; i<3*nb_couleur; i+=3){
        //on écrit i_c
        fwrite(&identifiant[i], 1, sizeof(identifiant[i]), doc);
        //écriture de nos deux i_h
        uint8_t ih = (identifiant[i+1]<<4)+//ih dc
                     identifiant[i+2];//ih ac
        fwrite(&ih, 1, sizeof(ih), doc);
    }
    //on écrit la fin de la balise
    unsigned char fin = 0;
    fwrite(&fin, 1, sizeof(fin), doc);
    fin = (char) 63;
    fwrite(&fin, 1, sizeof(fin), doc);
    fin = 0;
    fwrite(&fin, 1, sizeof(fin), doc);
}

/*écriture balise EOI */
void ecrit_EOI(FILE *doc){
    ecrit_2octet(0xffd9, doc);
}


/*
Fonction qui écrit un octet dans le fichier 
si la variable globale octet_prepa contient un octet complet à écrire
*/
static void ecrit_octet_si_plein(FILE *doc){
    if(occup == 8){
        fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
        if(octet_prepa == 0xff){
            occup = 0;
            octet_prepa = 0;
            fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
            return;
        }
        occup = 0;
        octet_prepa = 0;
    }
}


/*
Permet d'écrire un code de huffman sur un nombre de bits donnés dans le fichier
en mettant à jour la variable globale octet_prepa
*/
static void ecrit_code(FILE *doc, uint16_t code, uint8_t nb_bit){
    uint16_t masque = 0;
    //si on doit juste compléter notre octet
    if(occup + nb_bit <= 8){
        octet_prepa += code << (8-occup-nb_bit);
        occup += nb_bit;
        ecrit_octet_si_plein(doc);
        return;
    }
    //on collecte les bits de poids fort
    for(uint8_t i =0; i<8-occup; i++){
        masque += pow(2, nb_bit-1-i);
    }
    //on remplit notre octet que l'on écrit
    octet_prepa += (code & masque)>>(nb_bit - (8-occup));
    fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
    if(octet_prepa == 0xff){
        octet_prepa = 0;
        fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
    }
    octet_prepa = 0;
    masque = 0;

    //si on a de quoi écrire plus que 1 octet
    uint8_t nb_bit_collecter = 8-occup;

    //on écrit le nombre d'octets complets présents
    for(uint8_t i = 0; i<(nb_bit - (8-occup))/8; i++){
        //on collecte notre octet de poids fort
        for(uint8_t j =0; j<8; j++){masque += pow(2, nb_bit-nb_bit_collecter-j-1);}
        nb_bit_collecter += 8;
        //on remplit notre octet que l'on écrit
        octet_prepa += (code & masque)>>(nb_bit - nb_bit_collecter);
        fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
        if(octet_prepa == 0xff){
            octet_prepa = 0;
            fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);
        }
        octet_prepa = 0;
        masque = 0;
    }

    //on collecte les derniers bits au besoin : nb_bit - (8-occup)%8
    for(uint8_t i =0; i<(nb_bit - (8-occup))%8; i++){
        masque += pow(2, i);
    }
    octet_prepa = (code & masque)<<(8-(nb_bit - (8-occup))%8);
    occup = (nb_bit - (8-occup))%8;
}

/*écrit si possible octet et le met à jour */
void ecrit_bitstream(FILE *doc,
                     uint16_t code_huffman,
                     uint8_t nb_bit_h,
                     uint16_t indexe,
                     uint8_t nb_bit_index){
    ecrit_octet_si_plein(doc);
    ecrit_code(doc, code_huffman, nb_bit_h);
    ecrit_code(doc, indexe, nb_bit_index);
}

/* force l'écriture d'un bitstream*/
void force_ecriture(FILE *doc){
    if(occup != 0){fwrite(&octet_prepa, 1, sizeof(octet_prepa), doc);}
}
