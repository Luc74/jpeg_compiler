#include <mon_huffman.h>


/*
Une classe de tables de Huffman constituée d'un tableau de codages et de sa taille.
*/
struct Table{
    uint16_t taille;
    struct Codage *tableau;
};

/*
Initialise un doublet.
*/
struct Codage *creer_codage(uint8_t valeur, uint16_t code, uint8_t nb_bits){
    struct Codage *codage = malloc(sizeof(struct doublet *));
    codage->valeur = valeur;
    codage->code = code;
    codage->nb_bits = nb_bits;
    return codage;
}

/*
Fonction de libération de la mémoire associée à la table:
*/
void free_table(struct Table *table){
    free(table->tableau);
    free(table);
}

/*
Fonction qui permet de récupérer l'entier suivant dans la table
*/
static int maj_numero(int actuel, int dernier_bit){
    // On veut ajouter un 1, on reste dans la même branche
    if (dernier_bit==1){
        return (actuel*2+1);
    }
    // Le bit à ajouter vaut 0, on change de branche dans l'arbre de Huffman (une branche vers la droite)
    else{
        return 2*(actuel);
    }
}


/*
Construit la table du codage de Huffman correspondant aux paramètres:
    -dc_ac = 0 si on veut une table DC, 1 pour une AC
    -y_cb_cr = 0 si on veut le code sur Y, 1 pour Cb, 2 pour Cr
*/
struct Table *recup_table(int dc_ac, int y_cb_cr){
    
    // Initialisation:
    struct Table *resultat = malloc(sizeof(struct Table));
    resultat->tableau = calloc(sizeof(struct Codage), htables_nb_symbols[dc_ac][y_cb_cr]);
    uint16_t code;
    int actuel = 0;
    int dernier_bit = 0;
    uint8_t k = 0;
    int somme = 0;
    int i = 0;

    // On boucle sur le nombre de symboles à coder (connu):
    while (i < htables_nb_symbols[dc_ac][y_cb_cr]){

        // On parcours les symboles en retenant leur longueur:
        for (uint8_t j = 0; j < htables_nb_symb_per_lengths[dc_ac][y_cb_cr][k]; j++){

            // Ecriture du code suivant non occupé dans l'arbre (parcours en largeur):
            code = maj_numero(actuel, dernier_bit);
            resultat->tableau[i].valeur = htables_symbols[dc_ac][y_cb_cr][j + somme];
            resultat->tableau[i].code = code;
            resultat->tableau[i].nb_bits = k+1;

            // On a écrit un couple:
            i++;

            // On change de branche de l'arbre si la branche actuelle est remplie à la profondeur concernée:
            if (dernier_bit == 1){
                actuel = actuel + 1;
            }

            // Si on a écrit le dernier code avec un 0 à la fin, le suivant aura un 1 (et inversement):
            dernier_bit = 1-dernier_bit;
        }

        // Mise à jour des paramètres en fin de boucle:
        somme = somme + htables_nb_symb_per_lengths[dc_ac][y_cb_cr][k];
        k++;
        actuel = maj_numero(actuel, dernier_bit);
        dernier_bit = 0;
    }

    // Fin de la création de la table:
    resultat->taille = htables_nb_symbols[dc_ac][y_cb_cr];
    return resultat;
}


/*
Renvoie les coefficients encodés avec huffman:
    -dc_ac = 0 si on veut une table DC, 1 pour une AC
    -y_cb_cr = 0 si on veut le code sur Y, 1 pour Cb, 2 pour Cr
*/
struct Codage huffman(uint8_t coefficient, struct Table *tablehuff){

    // Initialisation:
    // struct Table *tablehuff = recup_table(dc_ac, y_cb_cr);
    struct Codage resultat;

    //On appelle une seule fois la fonction de récupération de table pour obtenir plus vite les coefficients:
    for (int j = 0; j < tablehuff->taille; j++){
        if (coefficient==tablehuff->tableau[j].valeur){
            resultat = tablehuff->tableau[j];
            break;
        }
    }

    return (resultat);
}


/*
Affiche un codage
*/
void affiche_codage(struct Codage *truc){
    printf("le code de %u est %u codé sur %u bits\n",truc->valeur, truc->code, truc->nb_bits);
}

/*
Affiche une table de huffman
*/
void affiche_table(struct Table *element){
    printf("Affichage de la table:\n");
    printf("taille de la table %i\n", element->taille);
    for (int i = 0; i < element->taille; i++){
        printf("le code de %u est %u codé sur %u bits\n",element->tableau[i].valeur, element->tableau[i].code, element->tableau[i].nb_bits);
    }
}
