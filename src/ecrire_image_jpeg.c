#include <ecrire_image_jpeg.h>
#include <quantification.h>
#include <assert.h>
#include <decoupage_bloc.h>
#include <MCU.h>

static uint8_t taille_table_echantillonage = 64;
static uint8_t dim_bloc = 8;

/* Écrit une en-tête jpeg*/
static void ecrit_en_tete(FILE *doc,
                    uint32_t nb_ligne,
                    uint32_t nb_colonne,
                    uint8_t nb_couleur,
                    uint8_t *echantillon,
                    uint8_t taille_table){
    assert((nb_couleur == 1)||(nb_couleur == 3));
    ecrit_balise_SOI(doc);
    ecrit_balise_APPO(doc);
    ecrit_DQT(doc,
              quantification_table_Y,
              taille_table,
              0);//car que le gris
    if(nb_couleur == 3){
        ecrit_DQT(doc,
              quantification_table_CbCr,
              taille_table,
              1);//car couleur
    }
    ecrit_SOFO(doc,
               8,
               nb_ligne,
               nb_colonne,
               nb_couleur,
               echantillon);

    //la dht de Y
    ecrit_DHT(doc, 0, 0);
    ecrit_DHT(doc, 1, 0);

    //les Cb et Cr si besoin (quand on est en couleur):
    if(nb_couleur == 3){
        //les tables Cb et Cr sont identiques, donc on en écrit que 2 sur les 4
        ecrit_DHT(doc, 0, 1);
        ecrit_DHT(doc, 1, 1);
    }

    //quel que soit notre nombre de couleurs, les indices avec
    //dans l'ordre, par couleur : i_c, i_h pour DC, i_h pour AC
    //d'où le tableau suivant : {1, 0, 0, 2, 1, 1, 3, 1, 1}

    uint8_t id[] = {1, 0, 0, 2, 1, 1, 3, 1, 1};
    ecrit_SOS(doc, nb_couleur, id);
}

/*Ecriture des données brutes */
static int16_t ecrit_donnees(FILE *doc,
                      float *ligne_pixel,
                      int16_t ancien_pix,
                      uint8_t nb_pixel,
                      uint8_t type_Y_Cbcr){

    //DC
    int16_t pix = quantification(ligne_pixel[0],
                                 0,
                                 type_Y_Cbcr);
    int16_t sauv_pix = pix; //on le sauvegarde pour la prochaine magnitude DC


    uint8_t code_DC = codage_DC(pix, ancien_pix);
    struct Table *huff_table_DC = recup_table(0, type_Y_Cbcr);
    struct Codage huff = huffman(code_DC, huff_table_DC);
    free_table(huff_table_DC);
    uint16_t index = indexe(pix - ancien_pix, code_DC);
    ecrit_bitstream(doc, huff.code, huff.nb_bits, index, code_DC);

    //les AC
    //initialisation et récupération des variables:
    struct AC_RLE *code;
    uint8_t nb_balise_16_0 = 0;
    uint8_t nb_zero = 0;
    bool fin = false;
    struct Table *huff_table_AC = recup_table(1, type_Y_Cbcr);

    for(uint8_t i = 1; i<nb_pixel; i++){
        pix = quantification(ligne_pixel[i], i, type_Y_Cbcr);
        if(i == nb_pixel-1){fin = true;}
        code = codage_en_AC(pix, nb_zero, nb_balise_16_0, fin);
        if(code->a_ecrire){
            //on écrit nos balises de 16 zéros si besoin:
            for(uint8_t j = 0; j<code->nb_balises_0; j++){
                huff = huffman(0xf0, huff_table_AC);
                ecrit_bitstream(doc, huff.code, huff.nb_bits, 0, 0);
            }
            huff = huffman(code->octet, huff_table_AC);
            ecrit_bitstream(doc,
                            huff.code,
                            huff.nb_bits,
                            indexe(pix, code->magnitude),
                            code->magnitude);
            nb_zero = 0;
            nb_balise_16_0 = 0;
        }
        else{
            nb_balise_16_0 = code->nb_balises_0;
            nb_zero = code->octet;
        }
        free(code);
    }

    free_table(huff_table_AC);
    return sauv_pix;
}

/* Cette fonction permet d'écrire une ligne de MCU dans le fichier doc saisi en entrée. */

static int16_t *ecrit_ligne_mcu(FILE *doc,
                struct Image *image,
                struct Arguments *arg,
                uint8_t nb_bloc_horizontal,
                uint8_t nb_bloc_vertical,
                int16_t *ancien_pix,
                double *tab_cos){

    //déclaration des variables:
    struct Bloc *bloc_en_cour_dct;
    float *ligne_pix;
    uint8_t fin_l;
    uint8_t fin_k;
    struct Bloc *matrice_bloc[nb_bloc_vertical][nb_bloc_horizontal];
    struct Bloc ***matrice_echantilloner;

    //on parcourt nos blocs de 8 par 8:
    for(uint32_t i = 0; i<nouvelle_dim(image->nb_ligne, dim_bloc); i+=nb_bloc_vertical){
        for (uint32_t j = 0; j < nouvelle_dim(image->nb_colonne, dim_bloc); j+=nb_bloc_horizontal){
            for(uint8_t c = 0; c<image->nb_couleur; c++){
                //on récupère nos données:
                for(uint32_t k = i; k<i+nb_bloc_vertical; k++){
                    for(uint32_t l = j; l<j+nb_bloc_horizontal; l++){
                        matrice_bloc[k-i][l-j] = recuperation_bloc(image, dim_bloc, dim_bloc, k, l, c);
                    }
                }
                //on échantillonne  les blocs:
                switch (c)
                {
                case 0:
                    fin_k = get_v1(arg);
                    fin_l = get_h1(arg);
                    break;
                case 1:
                    fin_k = get_v2(arg);
                    fin_l = get_h2(arg);
                    break;
                case 2:
                    fin_k = get_v3(arg);
                    fin_l = get_h3(arg);
                    break;
                }
                matrice_echantilloner = mcu(nb_bloc_horizontal,
                                            nb_bloc_vertical,
                                            matrice_bloc,
                                            fin_l,
                                            fin_k);

                //on écrit nos blocs échantillonés; on en profite pour libérer certains des blocs de départ:
                for(uint8_t k = 0; k<fin_k; k++){
                    for(uint8_t l = 0; l<fin_l; l++){
                        //calcul de la dct et écriture d'un bloc:
                        bloc_en_cour_dct = dct_8x8(tab_cos, matrice_echantilloner[k][l]);
                        free_bloc(matrice_echantilloner[k][l]);
                        ligne_pix = zigzag_1_MCU(bloc_en_cour_dct);
                        free_bloc(bloc_en_cour_dct);
                        ancien_pix[c] = ecrit_donnees(doc,
                                                    ligne_pix,
                                                    ancien_pix[c],
                                                    dim_bloc*dim_bloc,
                                                    c);
                        free(ligne_pix);
                    }
                    free(matrice_echantilloner[k]);
                }
                free(matrice_echantilloner);

                // On finit de libérer notre matrice de blocs:
                for(uint8_t k = 0; k<nb_bloc_vertical; k++){
                    for(uint8_t l = 0; l<nb_bloc_horizontal; l++){
                        free_bloc(matrice_bloc[k][l]);
                    }
                }
                
            }
        }
    }
    return ancien_pix;
}


/*
Une fonction qui lit une ligne de mcu dans le fichier source et l'écrit dans l'image.
*/

static void lit_une_ligne(struct Image *image,
                        FILE *fichier){

    /* On lit les pixels par octet*/
    // Cas de la couleur:
    if(image->nb_couleur == 3){
        uint32_t rouge;
        uint32_t vert;
        uint32_t bleu;

        // Conversion RGB->YCbCr:
        for(uint32_t i = 0; i<image->nb_colonne*image->nb_ligne; i++){
            rouge = fgetc(fichier);
            vert = fgetc(fichier);
            bleu = fgetc(fichier);
            image->pixel[i/image->nb_colonne][i%image->nb_colonne].Y = 0.299*rouge
                                                        +0.587*vert
                                                        +0.114*bleu;
            image->pixel[i/image->nb_colonne][i%image->nb_colonne].Cb = -0.1687*rouge
                                                        -0.3313*vert
                                                        +0.5*bleu
                                                        +128;
            image->pixel[i/image->nb_colonne][i%image->nb_colonne].Cr = 0.5*rouge
                                                        -0.4187*vert
                                                        -0.0813*bleu
                                                        +128;
        }
    }
    // Cas du noir et blanc:
    else{
        uint32_t octet;
        for(uint32_t i = 0; i<image->nb_colonne*image->nb_ligne; i++){
            octet = fgetc(fichier);
            image->pixel[i/image->nb_colonne][i%image->nb_colonne].Y = (float) octet;
        }
    }
}


/*
Une fonction pour lire un entier dans un fichier:
*/
static uint32_t lecture_numero(FILE *fichier){
    uint32_t num = 0;
    char chara = fgetc(fichier);
    while(isdigit(chara)){
        num *= 10;
        num += (uint32_t)chara - 48;
        chara = fgetc(fichier);
    }
    return num;
}

/* La fonction principale de notre projet, qui permet d'écrire notre fichier JPEG après avoir lu l'image d'entrée. */

void ecrit_image(struct Arguments *arg,
                uint8_t nb_bloc_horizontal,
                uint8_t nb_bloc_vertical){
    
    
    // Lecture de l'en-tête de l'image:

    //caractère courant de la lecture de l'en-tête:
    char chara;
    //chaine de caratères qui sert a passer des lignes de l'en-tête:
    char chaine[20] = "";
    //notre fichier
    FILE *fichier = fopen(get_origin_file(arg), "r" );
    FILE *fichier_sortie = fopen(get_outfile(arg), "w+");

    uint8_t nb_couleur;

    // Lecture de l'en-tete pour savoir si on a de la couleur:
    fgetc(fichier);//on passe le P
    chara = fgetc(fichier); //on passe le chiffre
    if(chara == '5'){
        nb_couleur = 1;
        }
    else{
        if (chara == '6'){
            nb_couleur = 3;
        }
        else{
            printf("Le fichier n'est pas au bon format (on a pas reconnu P5 ou P6");
            return;
        }
        }
    fgetc(fichier); //on passe le \n

    // Lecture de la largeur:
    uint32_t nb_colonne = lecture_numero(fichier);

    // Lecture de la hauteur:
    uint32_t nb_ligne = lecture_numero(fichier);
    
    // Préparation de l'écriture de l'en-tête avec, par composante:
    // (fact_horizontal, fact_vertical, i_q)
    uint8_t echantillonage[] = {get_h1(arg), get_v1(arg), 0, get_h2(arg), get_v2(arg), 1, get_h3(arg), get_v3(arg), 1};
    
    // Ecriture de l'en tete:
    ecrit_en_tete(fichier_sortie,
                  nb_ligne,
                  nb_colonne,
                  nb_couleur,
                  echantillonage,
                  taille_table_echantillonage);


    // Lecture/écriture des données brutes:
    int16_t ancien_pix[] = {0, 0, 0};
    uint8_t v_max = fmax(fmax(get_v1(arg), get_v2(arg)), get_v3(arg));
    fgets(chaine, 20, fichier);//on passe le nombre qui indique le codage qui fait 4 caratère

    // Récupération de la table des cosinus:
    double *tab_cos = recup_vet_cons_bloc8x8();

    for(uint32_t i_mcu = 0; i_mcu*dim_bloc*v_max < nb_ligne; i_mcu++){
        struct Image *image = init_image(fmin(dim_bloc*v_max, nb_ligne-i_mcu*dim_bloc*v_max), nb_colonne, nb_couleur);
        
        // Lecture par ligne de MCU:
        lit_une_ligne(image,
                    fichier);
        
        // Ecriture par ligne de MCU:
        int16_t *copie = ecrit_ligne_mcu(fichier_sortie,
                                            image,
                                            arg,
                                            nb_bloc_horizontal,
                                            nb_bloc_vertical,
                                            ancien_pix,
                                            tab_cos);
        free_image(image);
        for (uint8_t i = 0; i < 3; i++){
            ancien_pix[i] = copie[i];
        }
    }

    // Libération des structures/fermeture des fichiers:
    free(tab_cos);
    force_ecriture(fichier_sortie);
    ecrit_EOI(fichier_sortie);
    fclose(fichier_sortie);
    fclose(fichier);
}
