#include <zigzag.h>


/*
Cette fonction prend en entrée un MCU (de taille variable)
Il renvoie un vecteur issu du parcours en zigzag du MCU (cf documentation pour le schéma)
*/
float *zigzag_1_MCU(struct Bloc *mcu){
    int k=0;
    float *res=calloc(mcu->hauteur * mcu->largeur, sizeof(float));
    for (int32_t i=0; i < (int32_t) (2*mcu->hauteur); i++){
        if (i%2==0){
            
            // On effectue un parcours des cases (i,0) à (0,i) en diagonale:
            for (int32_t j = fmax(1+i-(int32_t) mcu->hauteur,0); j <= fmin(i,(int32_t) mcu->largeur-1); j++){
                res[k] = mcu->matrice[i-j][j];
                k++;
            }
        }
        else{

            // On effectue un parcours des cases (0,i) à (i,0) en diagonale:
            for (int32_t j = fmax(1+i-(int32_t) mcu->hauteur,0); j <= fmin(i,(int32_t) mcu->hauteur-1); j++){
                res[k] = mcu->matrice[j][i-j];
                k++;
            }
        }
    }
    return res;
}
