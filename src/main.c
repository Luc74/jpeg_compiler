#include <ecrire_image_jpeg.h>
#include <lecture.h>


int main(int argc, char *argv[]){

    // Récupération des arguments:
    struct Arguments *arg = lecture(argv, argc);

    if(arg != NULL){
        ecrit_image(arg,
                    get_h1(arg),
                    get_v1(arg));
        free_arguments(arg);
    }
}
