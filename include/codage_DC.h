#ifndef CODAGE_DC_H_
#define CODAGE_DC_H_

#include <ImageYCbCr.h>




/*
Fournit la classe de magnitude du numéro donné en argument.
*/
extern uint8_t magnitude(int numero);


/*
Renvoie la composante DC encodée d'un bloc 8x8 à partir du vecteur contenant les composantes du bloc.
*/
extern uint8_t codage_DC(int32_t pixel, int32_t ancien_dc);


/*
Renvoie l'index de coef dans la classe de magnitude magnitude
*/
extern uint16_t indexe(int16_t coef, uint8_t magnitude);

#endif /* _CODAGE_DC_H_ */
