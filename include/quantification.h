#ifndef QUANTIFICATION_H_
#define QUANTIFICATION_H_

#include <qtables.h>
#include <ImageYCbCr.h>



/*
La fonction qui se charge de la quantification d'un MCU lu en zigzag
*/
int16_t quantification(float param, uint8_t indice, uint8_t Y_CbCr);

#endif /* _QUANTIFICATION_H_ */
