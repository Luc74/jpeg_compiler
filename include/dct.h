#ifndef DCT_H_
#define DCG_H_

#include <decoupage_bloc.h>


/*
Fonction permettant de libérer la mémoire associée au tableau créé ci-dessus:
*/
extern void free_tableau(float **tab, uint8_t taille_bloc);

/*récipère le tableau de constante demander par la dct */
extern double *recup_vet_cons_bloc8x8();

/* calcule la dct */
extern struct Bloc *dct_8x8(double *constante, struct Bloc *bloc);

#endif /* _DCT_H_ */
