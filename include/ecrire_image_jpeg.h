#ifndef ECRIR_IMAGE_JPEG_H
#define ECRIR_IMAGE_JPEG_H

#include <ImageYCbCr.h>
#include <dct.h>
#include <zigzag.h>
#include <AC_RLE.h>
#include <codage_DC.h>
#include <lecture.h>




/*Écrit une image JPEG dans doc a partir de la structure image */
void ecrit_image(struct Arguments *arg,
                uint8_t nb_bloc_horizontal,
                uint8_t nb_bloc_vertical);

#endif /* IMAGE_H */
