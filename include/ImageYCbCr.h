#ifndef IMAGEYCBCR_H
#define IMAGEYCBCR_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <netinet/in.h>
#include <uchar.h>
#include <stdbool.h>
#include <mon_huffman.h>

/* Strucuture contenant les corrdonner dans un format Y Cb Cr*/
struct Pixel_YCbCr{
    float Y;
    float Cr;
    float Cb;
};

/* Strucuture contenant un tableau de pixel et sa taille*/
struct Image{
    uint8_t nb_couleur;
    uint32_t nb_ligne;
    uint32_t nb_colonne;
    struct Pixel_YCbCr** pixel;
};

/* cree une nouvelle image*/
extern struct Image *init_image(uint32_t nb_ligne, uint32_t nb_colonne, uint8_t nb_couleure);

/* cree un image de YCbCr depuis l'addresse d'un fichier .pgm*/
extern struct Image *chargement_RGB_YCbCr(char *nom_fichier);

/*libere la memoire associer a l'image*/
extern void free_image(struct Image *image);

/* affiche la luminosite d'une image*/
extern void affiche(struct Image *image);

/* ecrit une balsie SOI est vide le fichier d'ecriture si il n'etait pas vide*/
extern void ecrit_balise_SOI(FILE* doc);

/*ecrit la balise APPO dans le fichier doc*/
extern void ecrit_balise_APPO(FILE* doc);

/*ecrit une table de quantification */
extern void ecrit_DQT(FILE* doc,
                      uint8_t *qtables,
                      uint8_t taille_table,
                      uint8_t indice);

/* ecrit une balise SOFO */
extern void ecrit_SOFO(FILE* doc,
                       uint8_t precision,
                       uint32_t hauteur, 
                       uint32_t largeur,
                       uint8_t nb_couleur,
                       uint8_t *facteur_echantillonage);

/* ecrit une balise DHT*/
extern void ecrit_DHT(FILE *doc,
                      uint8_t type_table,
                      uint8_t indice_couleur);

/* ecrit une baslise SOS*/
extern void ecrit_SOS(FILE *doc,
                      uint8_t nb_couleur,
                      uint8_t *identifiant);

/*ecrit balise EOI */
extern void ecrit_EOI(FILE *doc);

/*ecrit si possible octet et le met a jour */
extern void ecrit_bitstream(FILE *doc,
                            uint16_t code_huffman,
                            uint8_t nb_bit_h,
                            uint16_t indexe,
                            uint8_t nb_bit_index);

/* force l'ecriture d'un bitstream*/
extern void force_ecriture(FILE *doc);


#endif /* IMAGE_H */