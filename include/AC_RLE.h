#ifndef AC_RLE_H_
#define AC_RLE_H_

#include <ImageYCbCr.h>
#include <codage_DC.h>

struct AC_RLE
{
    bool a_ecrire;
    uint8_t octet;
    uint8_t magnitude;
    uint8_t nb_balises_0;
};

/* 
crée et renvoie une struct AC_RLE indiquant si on doit écrire son contenu et si c'est le cas le code a écrire.
Si on ne doit pas l'écrire, son code contient le nombre de zéros lus.
 */
extern struct AC_RLE *codage_en_AC(int16_t pixel,
                                   uint8_t nb_zero,
                                   uint8_t nb_balises_0,
                                   bool fin_bloc);

#endif /* _AC_RLE_H_ */