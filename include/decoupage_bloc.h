#ifndef DECOUPAGE_bloc_H
#define DECOUPAGE_bloc_H

#include <ImageYCbCr.h>


struct Bloc{
    uint32_t hauteur;
    uint32_t largeur;
    float **matrice;
};


/*
Initialise un bloc vide
*/
extern struct Bloc *creer_bloc(uint32_t hauteur,
                               uint32_t longueur);

/*
Initialise une Image_3d vide
*/
extern struct Bloc *creer_bloc(uint32_t hauteur, uint32_t longueur);

/*
Libération de mémoire pour un Bloc
*/
extern void free_bloc(struct Bloc *bloc);

/*
Affiche un Bloc
*/
extern void affiche_bloc(struct Bloc *bloc);

/*
Renvoie la dimension (en pixels et pour 1D) de l'image issue de l'agrandissement
d'une image de dimension dim_init pour des blocs de dimension dim_blocs et de taille taille_bloc
*/
extern uint32_t nouvelle_dim(uint32_t dim_init, uint32_t dim_bloc);


/*
Renvoie la coordonnée d'un pixel (dans l'image de dimension dim_init) 
ayant les mêmes caractéristiques que le pixel d'indice indice dans l'image étendue.
*/
extern uint32_t extension(uint32_t dim_init,
                        uint32_t indice);

/*
Une fonction qui récupère les bloc depuis l'image originale (longueur bloc et hauteur nloc sont les dimensions des bloc)
*/

extern struct Bloc *recuperation_bloc(struct Image *image_originale,
                                uint32_t hauteur_bloc,
                                uint32_t longueur_bloc,
                                uint32_t i_bloc,
                                uint32_t j_bloc,
                                uint8_t Y_Cb_Cr
                                );


#endif /* DECOUPAGE_bloc_H */