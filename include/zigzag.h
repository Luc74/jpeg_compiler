#ifndef ZIGZAG_H_
#define ZIGZAG_H_

#include<decoupage_bloc.h>

/*
Cette fonction prend en entrée un MCU (tableau de taille 8x8)
Il renvoie un vecteur issu du parcours en zigzag du MCU (cf documentation pour le schéma)
*/
extern float *zigzag_1_MCU(struct Bloc *mcu);

#endif /* _ZIGZAG_H_ */