#ifndef MON_HUFFMAN_H
#define MON_HUFFMAN_H

#include <htables.h>
#include <ImageYCbCr.h>

/*
Une classe de coublets qui contiennent un code et une valeur (asociées dans le code de Huffman)
*/
struct Codage{
    uint8_t valeur;
    uint16_t code;
    uint8_t nb_bits;
};


/*
Une classe de tables de Huffman.
*/
struct Table;

/*
Initialise un doublet.
*/
extern struct Codage *creer_codage(uint8_t valeur, uint16_t code, uint8_t nb_bits);

/*
Fonction de libération de la mémoire associée à la table:
*/
void free_table(struct Table *table);

/*
Renvoie les coefficients encodés avec huffman:
    -dc_ac = 0 si on veut une table DC, 1 pour une AC
    -y_cb_cr = 0 si on veut le code sur Y, 1 pour Cb, 2 pour Cr
*/
extern struct Codage huffman(uint8_t coefficient, struct Table *tablehuff);

/*
Construit la table du codage de huffman correspondant aux paramètres:
    -dc_ac = 0 si on veut une table DC, 1 pour une AC
    -y_cb_cr = 0 si on veut le code sur Y, 1 pour Cb, 2 pour Cr
*/
extern struct Table *recup_table(int dc_ac, int y_cb_cr);
/*
Affiche une table de huffman
*/
extern void affiche_table(struct Table *element);

/*
Affiche un codage
*/
extern void affiche_codage(struct Codage *truc);

/*
free la table
*/
extern void free_table(struct Table *table);

#endif /* _MON_HUFFMAN_H_ */