#ifndef MCU_H
#define MCU_H

#include <ImageYCbCr.h>
#include <decoupage_bloc.h>

/* Prend, en entrée, un tableau de MCU (images).
Créé un tableau contenant les blocs qui contiendront une composante Cb.
h et v représentent, respectivement, les "colonnes" et "les lignes".  */

extern struct Bloc ***mcu(uint8_t nb_colonnes_init,
                          uint8_t nb_lignes_init,
                          struct Bloc *blocs_init[nb_lignes_init][nb_colonnes_init],
                          uint8_t h,
                          uint8_t v);


#endif /* MCU_H */