#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


#ifndef LECTURE_H
#define LECTURE_H


struct Arguments;

/*
Récupère h1
*/
extern uint8_t get_h1(struct Arguments *arg);

/*
Récupère v1
*/
extern uint8_t get_v1(struct Arguments *arg);

/*
Récupère h2
*/
extern uint8_t get_h2(struct Arguments *arg);

/*
Récupère h3
*/
extern uint8_t get_h3(struct Arguments *arg);

/*
Récupère v2
*/
extern uint8_t get_v2(struct Arguments *arg);

/*
Récupère v3
*/
extern uint8_t get_v3(struct Arguments *arg);

/*
Récupère origin_file
*/
extern char *get_origin_file(struct Arguments *arg);

/*
Récupère outfile
*/
extern char *get_outfile(struct Arguments *arg);

/*
Récupère verbose
*/
extern bool get_verbose(struct Arguments *arg);

/*
Libère la structure arguments
*/
extern void free_arguments(struct Arguments *arg);

/*
Récupère les arguments en entrée
*/
extern struct Arguments *lecture(char *argv[], int argc);


#endif /* LECTURE_H */